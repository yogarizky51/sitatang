@extends('admin.layouts.master')

@section('title', 'Dashboard')

@section('content-title', 'Dashboard')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Sistem Tanda Tangan Digital PTIK</h3>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <h2>Selamat Datang {{ Auth::user()->nama }}</h2>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            @if (Auth::user()->role_id_utama == 3)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $adminsetuju }}</h3>

                            <p>Sebagai Koorprodi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-email"></i>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $pengajuan }}</h3>

                            <p>Diajukan</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-email"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            @endif
            @if (Auth::user()->role_id_utama == 2)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-maroon">
                        <div class="inner">
                            <h3>{{ $admin }}</h3>

                            <p>Untuk Koorprodi</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-thumbs-up"></i>
                        </div>
                    </div>
                </div>
            @endif
            @if (Auth::user()->role_id_utama == 2)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{ $dilanjutkankekoorprodi }}</h3>

                            <p>Dilanjutkan ke Koorprodi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                    </div>
                </div>
            @endif
            @if (Auth::user()->role_id_utama == 3)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{ $koordosen }}</h3>
                            <p>Sebagai Dosen</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-email"></i>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ $setuju }}</h3>

                        <p>Disetujui</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{ $ditolak }}</h3>

                        <p>Ditolak</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-close"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
