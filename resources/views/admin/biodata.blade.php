@extends('admin.layouts.master')

@section('title', 'Biodata')

@section('content-title', 'Biodata')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row d-flex justify-content-center">
                                <h4 class="mb-0 ">Ubah Biodata</h4>
                            </div>
                            <hr>
                            <div class="card-body">
                                @foreach ($users as $user)
                                    @if ($user->pengguna_id == Auth::user()->pengguna_id)
                                        <form action="/edit-biodata/{{ $user->pengguna_id }}" method="post">
                                            @csrf
                                            @method('put')
                                            {{-- <input type="hidden" class="form-control form-control-line" name="id"
                                                    value="{{ $user->pengguna_id }}"> --}}
                                            <div class="form-group">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama" id="nama"
                                                    placeholder="Nama Lengkap" value="{{ $user->nama }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" name="email" id="email"
                                                    placeholder="Email" value="{{ $user->email }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="nip">NIDN</label>
                                                <input type="text" class="form-control" name="nip" id="nip"
                                                    placeholder="NIP" value="{{ $user->biodata->nip }}" disabled>
                                            </div>
                                            @foreach ($akses as $aks)
                                                @if ($aks->pengguna_id == $user->pengguna_id)
                                                    <div class="form-group">
                                                        <label for="role">Role</label>
                                                        <input type="text" class="form-control" name="role"
                                                            id="role" placeholder="Role" value="{{ $aks->role->nama }}"
                                                            disabled>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <button type="submit" class="mt-2 btn btn-primary">Ubah</button>
                                        </form>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row d-flex justify-content-center">
                                <h4 class="mb-0 ">Ubah Password</h4>
                            </div>
                            <hr>
                            @foreach ($users as $user)
                                @if ($user->pengguna_id == Auth::user()->pengguna_id)
                                    <form action="/edit-pass/{{ $user->pengguna_id }}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="password">Password Baru</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="password" class="form-control" name="password_confirmation"
                                                id="password_confirmation" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                    </form>
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
