<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
        <img src="{{ asset('images/contoh-qr.png') }}" alt="AdminLTE Logo" class="brand-image  elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Sitatang</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('logo.png') }}" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->nama }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/dashboard"
                        class="nav-link {{ request()->is('dashboard') ? 'active' : '' }} {{ request()->is('dashboard') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/biodata" class="nav-link {{ request()->is('biodata') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Biodata
                        </p>
                    </a>
                </li>
                @if (auth()->user()->role_id_utama == '1' || auth()->user()->role_id_utama == '2')
                    <li class="nav-item">
                        <a href="/manajemen-user"
                            class="nav-link {{ request()->is('manajemen-user') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Manajemen User
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id_utama == '1' || auth()->user()->role_id_utama == '2')
                    <li class="nav-item">
                        <a href="/koorprodi-permohonan"
                            class="nav-link {{ request()->is('koorprodi-permohonan') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-thumbs-up"></i>
                            <p>
                                Permohonan Untuk Koorprodi
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id_utama == '1' || auth()->user()->role_id_utama == '3')
                    <li class="nav-item">
                        <a href="/daftar-permohonan-koorprodi"
                            class="nav-link {{ request()->is('daftar-permohonan-koorprodi') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Permohonan Pada Koorprodi
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id_utama == '1' || auth()->user()->role_id_utama == '3')
                    <li class="nav-item">
                        <a href="/daftar-permohonan-koordosen"
                            class="nav-link {{ request()->is('daftar-permohonan-koordosen') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Permohonan Pada Dosen
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id_utama != '3')
                    <li class="nav-item">
                        <a href="/daftar-permohonan"
                            class="nav-link {{ request()->is('daftar-permohonan') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Permohonan Masuk
                            </p>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="/permohonan-selesai"
                        class="nav-link {{ request()->is('permohonan-selesai') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-check"></i>
                        <p>
                            Permohonan Selesai
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/logout" class="nav-link bg-danger">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
