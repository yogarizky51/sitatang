@extends('admin.layouts.master')

@section('title', 'Manajemen User')

@section('content-title', 'Manajemen User')

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Daftar User</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <button type="button" class="mb-2 btn btn-primary" data-toggle="modal" data-target="#tambah-user">
                Tambah Pengguna
            </button>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        {{-- <th>No</th> --}}
                        <th>Nama</th>
                        <th>Email</th>
                        <th>NIDN</th>
                        <th>Jabatan Utama</th>
                        <th>Jabatan Merangkap</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                        @if ($user->role_id_utama != 1)
                            <tr>
                                {{-- <td>{{ $user->index }}</td> --}}
                                <td>{{ $user->nama }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->biodata->nip }}</td>
                                <td>{{ $user->role_utama->nama }}</td>
                                @if ($user->role_id_kedua !== null)
                                    <td>{{ $user->role_kedua->nama }}</td>
                                @else
                                    <td>Tidak Ada</td>
                                @endif
                                {{-- @foreach ($akses as $a)
                                @if ($a->pengguna_id == $user->pengguna_id)
                                    <td>{{ $user->role_utama->nama }}</td>
                                @endif
                            @endforeach
                            @foreach ($akses as $a)
                                @if ($a->pengguna_id == $user->pengguna_id)
                                    @if ($a->role_kedua !== null)
                                        <td>{{ $user->role_kedua->nama }}</td>
                                    @else
                                        <td>Tidak ada</td>
                                    @endif
                                @endif
                            @endforeach --}}
                                @foreach ($akses as $a)
                                    @if ($a->pengguna_id == $user->pengguna_id)
                                        @if ($a->aktif == 1)
                                            <td>aktif</td>
                                        @else
                                            <td>tidak aktif</td>
                                        @endif
                                    @endif
                                @endforeach
                                <td>
                                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Edit">
                                        <button data-toggle="modal" data-target="#edit-user{{ $user->pengguna_id }}"
                                            type="button" class="btn waves-effect waves-light btn-xs btn-warning"
                                            data-id="" data-aktif="" data-roleid="">
                                            <span class="p-2 text-white">Edit</span>
                                        </button>
                                    </span>
                                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Hapus">
                                        <form action="/hapus-user/{{ $user->pengguna_id }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn waves-effect waves-light btn-xs btn-danger"
                                                onclick="return confirm('Apakah anda yakin ingin menghapus?')">
                                                <span class="p-2 text-white">Hapus</span>
                                            </button>
                                        </form>
                                        {{-- <button data-toggle="modal" data-target="#hapus" type="button"
                                        class="btn waves-effect waves-light btn-xs btn-danger">
                                        <span class="p-2 text-white">Hapus</span>
                                    </button> --}}
                                    </span>
                                </td>
                            </tr>
                        @endif
                    @empty
                        Data Kosong
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="modal fade" id="tambah-user">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Pengguna</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('tambah-user') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama" id="nama"
                                    placeholder="Nama Lengkap">
                                @error('nama')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="Email">
                                @error('email')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password"
                                    placeholder="Password">
                                @error('password')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="nip">NIDN</label>
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="NIDN">
                                @error('nip')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="role">Jabatan Utama</label>
                                <select class="form-control" style="width: 100%;" name="role_id_utama" id="role_id_utama"
                                    placeholder="role_id_utama" autocomplete="off">
                                    <option disabled selected="selected">Silahkan Pilih Role</option>
                                    @foreach ($role as $r)
                                        @if ($r->role_id != 1 && $r->role_id != 5)
                                            <option value="{{ $r->role_id }}">{{ $r->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id_utama')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="role">Jabatan Merangkap</label>
                                <select class="form-control" style="width: 100%;" name="role_id_kedua" id="role_id_kedua"
                                    placeholder="role_id_kedua" autocomplete="off">
                                    <option disabled selected="selected">Silahkan Pilih Role</option>
                                    @foreach ($role as $r)
                                        @if ($r->role_id == 4 || $r->role_id == 5)
                                            <option value="{{ $r->role_id }}" {{ $r->role_id == 5 ? 'selected' : '' }}>
                                                {{ $r->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id_kedua')
                                    <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                                <small class="text-dark">Kosongkan jika tidak merangkap jabatan</small>
                            </div>
                            <div class="form-group">
                                <label for="status" class="control-label">Status:</label>
                                <label class="custom-control custom-radio">
                                    <input id="radio1" value="1" name="aktif" type="radio"
                                        class="custom-control-input">
                                    <span class="custom-control-label">Aktif</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input id="radio2" value="0" name="aktif" type="radio"
                                        class="custom-control-input">
                                    <span class="custom-control-label">Tidak Aktif</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-success">
                            Tambah
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @foreach ($users as $user)
        <div class="modal fade" id="edit-user{{ $user->pengguna_id }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Pengguna</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/edit-user/{{ $user->pengguna_id }}" method="post">
                        @csrf
                        @method('put')
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="nama" id="nama"
                                        placeholder="Nama Lengkap" value="{{ old('nama') ?? $user->nama }}">
                                    @error('nama')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="Email" value="{{ old('email') ?? $user->email }}">
                                    @error('email')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nip">NIDN</label>
                                    <input type="text" class="form-control" name="nip" id="nip"
                                        placeholder="NIDN" value="{{ old('nip') ?? $user->biodata->nip }}">
                                    @error('nip')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role">Jabatan Utama</label>
                                    <select class="form-control" style="width: 100%;" name="role_id_utama"
                                        id="role_id_utama" placeholder="role_id_utama" autocomplete="off">
                                        <option disabled selected>Silahkan Pilih Role</option>
                                        @foreach ($role as $r)
                                            {{-- @if ($r->role_id) --}}
                                            {{-- @foreach ($akses as $ak) --}}
                                            {{-- @if ($ak->akses_id == $r->role_id) --}}
                                            @if ($r->role_id != 1 && $r->role_id != 5)
                                                <option value="{{ $r->role_id }}"
                                                    {{ $user->role_id_utama == $r->role_id ? 'selected' : '' }}>
                                                    {{ $r->nama }}
                                                </option>
                                            @endif
                                            {{-- @endif --}}
                                        @endforeach
                                        {{-- @endif --}}
                                        {{-- @endforeach --}}
                                    </select>
                                    @error('role_id_utama')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role">Jabatan Merangkap</label>
                                    <select class="form-control" style="width: 100%;" name="role_id_kedua"
                                        id="role_id_kedua" placeholder="role_id_kedua" autocomplete="off">
                                        <option disabled selected>Silahkan Pilih Role</option>
                                        @foreach ($role as $r)
                                            {{-- @if ($r->role_id) --}}
                                            {{-- @foreach ($akses as $ak) --}}
                                            {{-- @if ($ak->akses_id == $r->role_id) --}}
                                            @if ($r->role_id == 4 || $r->role_id == 5)
                                                <option value="{{ $r->role_id }}"
                                                    {{ $user->role_id_kedua == $r->role_id ? 'selected' : '' }}>
                                                    {{ $r->nama }}
                                                </option>
                                            @endif
                                            {{-- @endif --}}
                                        @endforeach
                                        {{-- @endif --}}
                                        {{-- @endforeach --}}
                                    </select>
                                    @error('role_id_kedua')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                    <small class="text-dark">Kosongkan jika tidak merangkap jabatan</small>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="control-label">Status:</label>
                                    @foreach ($akses as $ak)
                                        @if ($ak->pengguna_id == $user->pengguna_id)
                                            <label class="custom-control custom-radio">
                                                <input id="radio1" value="1" name="aktif" type="radio"
                                                    class="custom-control-input" {{ $ak->aktif == '1' ? 'checked' : '' }}>
                                                <span class="custom-control-label">Aktif</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input id="radio2" value="0" name="aktif" type="radio"
                                                    class="custom-control-input" {{ $ak->aktif == '0' ? 'checked' : '' }}>
                                                <span class="custom-control-label">Tidak Aktif</span>
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-success">
                                Ubah
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- <div id="hapus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form method="POST" action="{{ url('users.delete') }}">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <h4 id="delete"></h4>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="iduser" name="id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">Hapus</button>
                    </div>
                </div>
            </form>
        </div>
    </div> --}}
    <!-- /.modal -->
@endsection

@push('scripts')
    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["csv", "excel", "pdf"]
                "buttons": [{
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 4]
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 4]
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 4]
                        }
                    }
                ],
                "language": {
                    "emptyTable": "Data tidak ditemukan"
                },
            })
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        // $('#edit').on('show.bs.modal', function(e) {
        //     // console.log(masuk);
        //     let button = $(e.relatedTarget);
        //     console.log(button.data('aactive'));
        //     $('#id').val(button.data('id'));
        //     $('#roleid').val(button.data('roleid'));
        //     $('#aactive_s').val(button.data('aactive'));
        //     $('#aactive').val(button.data('aactive'));
        //     let id_jenis = button.data('jenis')
        //     $('#radio1').iCheck('check');
        //     var $radios = $('input:radio[name=status]');
        //     if (button.data('aactive') == 1) {
        //         console.log("masuk if")
        //         $radios.filter('[value=1]').prop('checked', true);
        //     } else {
        //         console.log("masuk else")
        //         $radios.filter('[value=0]').prop('checked', true);
        //     }
        // });
    </script>
@endpush
