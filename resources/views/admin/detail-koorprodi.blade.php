@extends('admin.layouts.master')

@section('title', 'Detail Permohonan')

@section('content-title', 'Detail Permohonan')

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Detail Permohonan</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-8 order-1 order-md-1">
                    {{-- <div class="row">
                        <div class="col-12 col-sm-4">
                            <div class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Estimated budget</span>
                                    <span class="info-box-number text-center text-muted mb-0">2300</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Total amount spent</span>
                                    <span class="info-box-number text-center text-muted mb-0">2000</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Estimated project duration</span>
                                    <span class="info-box-number text-center text-muted mb-0">20</span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-12">
                            <h4 class="mt-2 mb-4">Detail Surat</h4>
                            <div class="post">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Nama Pemohon</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $permohonan->nama_pemohon }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Email Pemohon</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $permohonan->email_pemohon }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Judul Surat:</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">{{ $permohonan->judul_surat }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Diajukan Pada Tanggal:</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                            {{ $permohonan->created_at->translatedFormat('d F Y H:i:s') }}
                                        </p>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0">Address</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="text-muted mb-0">Bay Area, San Francisco, CA</p>
                                        </div>
                                    </div> --}}
                            </div>

                            <div class="post">
                                <div class="user-block">
                                    <span class="ml-0 username">
                                        <span class="text-md">Pesan Pemohon Surat</span>
                                    </span>
                                    {{-- <span class="ml-0 description">Sent you a message - 3 days ago</span> --}}
                                </div>
                                <p>
                                    {{ $permohonan->pesan }}
                                </p>
                            </div>
                            <div class="post">
                                <div class="user-block">
                                    <span class="ml-0 username">
                                        <span class="text-md">File Terlampir</span>
                                    </span>
                                    {{-- <span class="ml-0 description">Sent you a message - 3 days ago</span> --}}
                                </div>
                                <div>
                                    <a href="/download/{{ $dokumen->rename }}" class="text-md"><i
                                            class="fas fa-link mr-1"></i> {{ $dokumen->file_name }}</a> <br> <br>
                                    @if ($attach)
                                        <a href="/download/{{ $attach->rename }}" class="text-md"><i
                                                class="fas fa-link mr-1"></i> {{ $attach->file_name }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($permohonan->status_id == 1)
                    <div class="col-lg-4 order-2 order-md-2">
                        <h3 class="text-success"><i class="fas fa-check-circle"></i> Persetujuan</h3>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-setuju">
                            Setuju
                        </button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-tidak">
                            Tidak Setuju
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="modal fade" id="modal-setuju">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-red">Perhatian !!!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin menyetujui permohonan ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <form action="/admin-setuju/{{ $permohonan->permohonan_id }}" method="post">
                        @csrf
                        {{-- <input type="hidden" name="status_id"> --}}
                        <button type="submit" class="btn btn-danger">Setuju</button>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-tidak">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-red">Perhatian !!!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/permohonan-tidak-setuju/{{ $permohonan->permohonan_id }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="note" class="col-form-label">Alasan Penolakan?</label>
                            <textarea class="form-control" name="note" id="note" placeholder="silahkan masukkan alasan penolakan disini"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-danger">Tidak Setuju</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
