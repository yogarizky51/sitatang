@extends('publik.layouts.master')

@section('title', 'Cara Menggunakan')

@section('content')
    <section class="impress_area p_120">
        <div class="container">
            <div class="impress_inner">
                <h2 class="text-center">
                    Silahkan Unduh File Cara Pemakaian Web <br>
                    Sistem Tanda Tangan Digital Berbasis QR Code <br>
                    Pada Link di Bawah ini
                </h2>
                <a class="banner_btn2" href="/panduan">Unduh PDF Pemakaian SiTaTang</a>
            </div>
        </div>
    </section>
@endsection
