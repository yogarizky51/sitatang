<footer class="footer_area p-5">
    <div class="container">
        <div class="row footer_inner">
            <div class="col-lg-6 col-sm-6">
                <aside class="f_widget ab_widget">
                    <div class="f_title">
                        <h3>Tentang SiTaTang</h3>
                    </div>
                    <p>Sistem Tanda Tangan (SiTaTang) Digital berbasis QR Code dibuat dengan harapan
                        dapat memberikan tanda tangan yang memiliki kemanan lebih tinggi sehingga
                        sulit untuk dimanipulasi oleh oknum tak bertanggungjawab
                    </p>
                </aside>
            </div>
            <div class="col-lg-6 col-sm-6">
                <aside>
                    <div class="f_title">
                        <h3>Copyright</h3>
                    </div>
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> Pendidikan Teknik Informatika dan Komputer | This template is made
                        with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com"
                            target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </aside>
            </div>
        </div>
    </div>
</footer>
