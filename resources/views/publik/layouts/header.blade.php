<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="/"><img src="{{ asset('images/SiTaTang-logo.png') }}"
                        alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSitatang"
                    aria-controls="navbarSitatang" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSitatang">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="/permohonan">Permohonan Dosen dan Admin</a></li>
                        <li class="nav-item"><a class="nav-link" href="/permohonan-koorprodi">Permohonan Koorprodi</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="/status">Cek Status</a>
                        <li class="nav-item"><a class="nav-link" href="/cara-pakai">Panduan</a>
                            {{-- <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Pages</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="price.html">Pricing</a>
                                <li class="nav-item"><a class="nav-link" href="elements.html">Elements</a></li>
                            </ul>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Blog</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
                                <li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li> --}}
                            @auth
                            <li class="nav-item"><a href="/dashboard" class="nav-link d-lg-none">Dashboard</a></li>
                        @else
                            <li class="nav-item"><a href="/login" class="nav-link d-lg-none">Login</a></li>
                        @endauth
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @auth
                            <li class="nav-item"><a href="/dashboard" class="tickets_btn">Dashboard</a></li>
                        @else
                            <li class="nav-item"><a href="/login" class="tickets_btn">Login</a></li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
