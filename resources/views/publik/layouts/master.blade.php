<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('images/contoh-qr.png') }}" type="image/png">
    <title>@yield('title')</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/assets_publik/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/lightbox/simpleLightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/nice-select/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/animate-css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/vendors/flaticon/flaticon.css') }}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('/assets_publik/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets_publik/css/custom.css') }}">
</head>

<body>

    <!--================Header Menu Area =================-->
    @include('publik.layouts.header')
    <!--================Header Menu Area =================-->
    @yield('content')
    <!--================Footer Area =================-->
    @include('publik.layouts.footer')
    <!--================End Footer Area =================-->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('/assets_publik/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/popper.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/stellar.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/lightbox/simpleLightbox.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/isotope/isotope-min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/vendors/counter-up/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/mail-script.js') }}"></script>
    @stack('scripts')
    @include('sweetalert::alert')

</html>
