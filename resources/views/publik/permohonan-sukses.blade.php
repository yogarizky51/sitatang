@extends('publik.layouts.master')

@section('title', 'Permohonan Sukses')

@section('content')
    <section class="impress_area p_120">
        <div class="container rounded bg-white p-5">
            <div class="impress_inner">
                <h2 class="text-center text-primary">
                    Permohonan sukses diajukan
                </h2>
                <h3 class="text-dark">
                    Token permohonan: {{ $pemohon->token_pemohon }}
                </h3>
                <h5 class="text-danger pt-3">
                    Token permohonan dikirim juga ke email yang dimasukkan pada form permohonan. <br>
                    Mohon simpan token tersebut agar bisa melakukan cek status permohonan.
                </h5>
            </div>
        </div>
    </section>
@endsection
