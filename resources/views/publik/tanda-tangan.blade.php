@extends('publik.layouts.master')

@section('title', 'Informasi Penandatangan')

@section('content')
    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Informasi Penandatangan</h2>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <h3 class="text-uppercase text-danger text-center p-3">Jika terdapat perbedaan informasi dengan surat fisik <br>
        maka informasi yang valid adalah yang terdapat pada sistem ini</h3>
    <section class="contact_area pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto text-left border border-info p-5">
                    <h4 class="text-dark text-center"> Informasi Pemohon Tanda Tangan:</h4>
                    <div class="form-group">
                        <h4>Nama Pemohon: {{ $dokumen->permohonan->nama_pemohon }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Diajukan Tanggal: {{ $dokumen->permohonan->created_at->translatedFormat('d F Y H:i:s') }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Kode Unik Permohonan: {{ $dokumen->permohonan->token_pemohon }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Keterangan Surat: {{ $dokumen->permohonan->judul_surat }}</h4>
                    </div>
                    <h4 class="text-dark text-center"> Informasi Pemberi Tanda Tangan:</h4>
                    <div class="form-group">
                        <h4>Nama Penandatangan: {{ $dokumen->permohonan->pengguna->nama }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Jabatan: {{ $dokumen->permohonan->role->nama }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Tanggal Disetujui: {{ $dokumen->permohonan->updated_at->translatedFormat('d F Y H:i:s') }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Kode Unik Permohonan: {{ $dokumen->permohonan->token_pemohon }}</h4>
                    </div>
                    <div class="form-group">
                        <h4>Keterangan Surat: {{ $dokumen->permohonan->judul_surat }}</h4>
                    </div>
                    <div class="float-left form-group pt-5">
                        <h4>File Surat Yang Diajukan: <br> <a
                                href="/download/{{ $dokumen->rename }}">{{ $dokumen->rename }}</a> <br>
                            @if ($attach)
                                <a href="/download/{{ $attach->rename }}">{{ $attach->rename }}</a>
                            @endif
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <!-- contact js -->
    <script src="{{ asset('/assets_publik/js/jquery.form.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/contact.js') }}"></script>
@endpush
