@extends('publik.layouts.master')

@section('title', 'Pengajuan Tanda Tangan')

@section('content')
    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Pengajuan Tanda Tangan</h2>
                    <div class="text-white">
                        <p>Silahkan isi formulir pengajuan</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Contact Area =================-->
    <section class="contact_area p-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{-- <form class="row contact_form border border-info p-5" action="contact_process.php" method="post"
                        id="contactForm" novalidate="novalidate"> --}}
                    <form action="/permohonan" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row contact_form ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nama Pemohon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control border border-info" id="nama"
                                        name="nama" placeholder="Nama Pemohon">
                                    @error('nama')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Email Pemohon<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control border border-info" id="email"
                                        name="email" placeholder="Email Pemohon">
                                    <small id="emailHelp" class="form-text text-gray">Pastikan email yang dimasukkan
                                        benar karena akan dikirimkan token.</small>
                                    @error('email')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="role">Diajukan Kepada<span class="text-danger">*</span></label>
                                    <select class="form-control border border-info" style="width: 100%;" name="pengguna_id"
                                        id="pengguna_id" placeholder="pengguna_id" autocomplete="off">
                                        <option disabled selected>Ajukan Kepada</option>
                                        @foreach ($rp as $r)
                                            {{-- @if ($r->role_id) --}}
                                            {{-- @foreach ($akses as $ak) --}}
                                            {{-- @if ($ak->akses_id == $r->role_id) --}}
                                            @if ($r->aktif == 1)
                                                @if ($r->pengguna->role_id_kedua != null)
                                                    @if ($r->pengguna->role_id_utama != 1)
                                                        <option value="{{ $r->pengguna->pengguna_id }}">
                                                            {{ $r->pengguna->nama }}
                                                        </option>
                                                    @endif
                                                @endif
                                            @endif
                                            {{-- @endif --}}
                                        @endforeach
                                        {{-- @endif --}}
                                        {{-- @endforeach --}}
                                    </select>
                                    @error('pengguna_id')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Pesan Pengajuan<span class="text-danger">*</span></label>
                                    <textarea class="form-control border border-info" name="pesan" id="pesan" rows="3"
                                        placeholder="Silahkan tulis pesan permohonan kepada pihak yang dituju"></textarea>
                                    @error('pesan')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Halaman yang ingin ditandangani<span
                                            class="text-danger">*</span></label><br>
                                    <input type="file" id="file1" name="file1" accept="application/pdf">
                                    @error('file1')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="">File/Surat Permohonan Keselurahan</label><br>
                                    <input type="file" id="file2" name="file2" accept="application/pdf">
                                    @error('file2')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <hr>
                                <div class="form-text">
                                    <ul class="p-0 text-danger">
                                        <li class="mt-2"><b class="text-danger">Jika file lebih dari 1 halaman</b>, upload
                                            halaman yang berisi kolom tanda tangan pada bagian <b
                                                class="text-danger">"Halaman yang ingin ditandatangani"</b> dan upload
                                            keseluruhan surat/permohonan pada bagian <b class="text-danger">"File/Surat
                                                Permohonan Keselurahan"</b>
                                        </li>
                                        <li class="mt-2"><b class="text-danger">Jika file hanya 1 halaman</b>, silahkan
                                            upload file yang sama pada bagian <b class="text-danger">"Halaman yang ingin
                                                ditandatangani"</b> dan <b class="text-danger">"File/Surat
                                                Permohonan Keselurahan"</b>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-12 text-right">
                                {{-- <button type="submit" value="submit" class="btn submit_btn">Send Message</button> --}}
                                <button type="submit" class="btn main_btn">Kirim Permohonan</button>
                            </div>
                        </div>
                    </form>

                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </section>
    <!--================Contact Area =================-->

    <!--================Contact Success and Error message Area =================-->
    {{-- <div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <p>Permohonan Sukses Dikirim</p>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- Modals error -->

    {{-- <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <p> Something went wrong </p>
                </div>
            </div>
        </div>
    </div> --}}
    <!--================End Contact Success and Error message Area =================-->
@endsection

@push('scripts')
    <!-- contact js -->
    <script src="{{ asset('/assets_publik/js/jquery.form.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/assets_publik/js/contact.js') }}"></script>
@endpush
