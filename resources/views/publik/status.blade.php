@extends('publik.layouts.master')

@section('title', 'Status Permohonan')

@section('content')
    <!--================Impress Area =================-->
    <section class="impress_area p_50">
        <div class="container">
            <div class="impress_inner">
                <h2>Cek status permohonan yang diajukan</h2>
                <p>Silahkan Masukkan Token yang didapatkan pada saat pengajuan</p>
            </div>
        </div>
    </section>
    <!--================End Impress Area =================-->
    <!--================Contact Area =================-->
    <section class="contact_area p-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row contact_form pt-5 pb-3 ">
                        <div class="col-md-12">
                            <form action="/status" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="">Masukkan Token Permohonan:</label>
                                    <input type="text" class="form-control border border-info" id="token"
                                        name="token" placeholder="Token">
                                    @error('token')
                                        <div class="text-danger"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                                <div class=" text-right">
                                    <button type="submit" value="submit" class="btn main_btn mt-4">Masukkan Token</button>
                                </div>
                            </form>
                            <div class="impress_area bg-white mt-5 rounded">
                                {{-- <div class="impress_inner p-3">
                            <h4 class="text-info">Status Permohonan Yaitu: <span class="text-danger">Ditolak</span> Atau
                                <span class="text-success">Selesai</span> Atau <span class="text-warning">Menunggu
                                    Persetujuan</span>
                            </h4>
                            <p class="text-justify text-dark pt-1"><span class="text-danger">Alasan Penolakan:</span> Lorem
                                ipsum
                                dolor sit amet consectetur adipisicing elit. Non mollitia qui assumenda laudantium corrupti
                                vel, autem voluptates minima magni ipsum. Ducimus minima harum
                                commodi ad vitae necessitatibus placeat veniam maiores.
                            </p>
                            <a class="main_btn" href="#">Unduh File Yang Disetujui</a>
                            <h4 class="text-info p-3">Mohon Maaf Token Tidak Ditemukan</h4>
                        </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <!--================Contact Area =================-->
@endsection
