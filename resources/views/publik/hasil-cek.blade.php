@extends('publik.layouts.master')

@section('title', 'Status Permohonan')

@section('content')
    <!--================Impress Area =================-->
    <section class="impress_area p_75">
        <div class="container">
            <div class="impress_inner">
                {{-- <h2>Cek status permohonan yang diajukan</h2> --}}
                <h3 class="text-white">Hasil pengecekan token yang dimasukkan</h3>
            </div>
        </div>
    </section>
    <!--================End Impress Area =================-->
    <!--================Contact Area =================-->
    <section class="contact_area pb_50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="impress_area bg-white rounded">
                        <div class="impress_inner">
                            <h4 class="text-info ">Status permohonan dengan keterangan dibawah ini: <br><br>
                                <ul>
                                    <li class="text-left text-dark">
                                        <h5>Diajukan oleh:
                                            {{ $permohonan->nama_pemohon }}</h5>
                                    </li> <br>
                                    <li class="text-left text-dark">
                                        <h5>Diajukan kepada:
                                            {{ $permohonan->pengguna->nama }}</h5>
                                    </li> <br>
                                    <li class="text-left text-dark">
                                        <h5>Judul
                                            surat: {{ $permohonan->judul_surat }}</h5>
                                    </li>
                                </ul>
                                @if ($permohonan->status_id == 1)
                                    Status: <span class="text-warning">Menunggu Persetujuan</span>
                                @elseif($permohonan->status_id == 2)
                                    Status: <span class="text-success">Disetujui</span>
                                @elseif($permohonan->status_id == 4)
                                    Status: <span class="text-warning">Disetujui Admin</span>
                                @else
                                    Status: <span class="text-danger">Ditolak</span>
                                @endif
                            </h4>
                            @if ($permohonan->status_id == 3 && $permohonan->note != null)
                                <p class="text-dark"><span class="text-danger">Alasan
                                        Penolakan: </span> <br>
                                    <textarea class="w-100" name="" id="" cols="40" rows="5" disabled>{{ $permohonan->note }}</textarea>
                                </p>
                            @endif
                            @if ($permohonan->status_id == 1 || $permohonan->status_id == 4)
                                <h4 class="text-danger mt-5">
                                    Jika pengajuan masih belum mendapat proses dalam waktu yang dirasa lama, Silahkan
                                    menghubungi yang bersangkutan untuk konfirmasi ulang
                                </h4>
                            @endif
                            @if ($permohonan->status_id == 2)
                                <form action="/dokumen-selesai/ {{ $permohonan->permohonan_id }}" method="post">
                                    @csrf
                                    <button type="button" class="mb-3 main_btn d-block" data-toggle="modal"
                                        data-target="#prevImage">Lihat
                                        Preview Posisi Tanda Tangan</button>
                                    <div class="form-group">
                                        <label for="role">Posisi Tanda Tangan:</label>
                                        <select class="form-control border border-info" style="width: 100%;"
                                            name="posisi_id" id="posisi_id" placeholder="posisi_id" autocomplete="off">
                                            <option disabled selected>Pilih Posisi</option>
                                            @foreach ($posisi as $p)
                                                <option value="{{ $p->posisi_id }}">
                                                    {{ $p->letak_posisi }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('posisi_id')
                                            <div class="text-danger"><small>{{ $message }}</small></div>
                                        @enderror
                                    </div>
                                    <div class="d-md-flex justify-content-between">
                                        <button type="submit" class="main_btn d-block">Unduh
                                            QR Code beserta dokumen</button>
                                        <a class="main_btn d-block mt-2 mt-md-0 custom-href"
                                            href="/download-qr-code/{{ $permohonan->qr_name }}">Unduh
                                            {{-- <button type="button"> </button> --}}
                                            QR Code saja</a>
                                    </div>
                                    <h4>
                                        <ul class="p-2">
                                            <li class="mt-2 text-left text-dark">Jika Posisi Tanda Tangan Tidak Berubah,
                                                <br>
                                                Mohon Untuk Melakukan Refresh Halaman
                                            </li>
                                            <li class="mt-2 text-left text-dark">Jika Posisi Tanda Tangan Masih Belum Sesuai
                                                Keinginan, Silahkan unduh QR-Code dan pasangkan pada berkas sesuai
                                                dengan posisi yang diinginkan</li>
                                        </ul>
                                    </h4>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Contact Area =================-->
    <div id="prevImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <img src="{{ asset('file_gambar/' . '[PREVIEW]_' . $permohonan->judul_surat . $permohonan->token_pemohon . '.jpg') }}"
                        class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection
