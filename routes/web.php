<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\PanduanController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\PermohonanController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerifikasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//AUTHENTICATION
Route::middleware('guest')->group(function () {
    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'login']);

    Route::get('lupa-password', [UserController::class, 'indexLupa'])->name('password.request');;
    Route::post('lupa-password', [UserController::class, 'storeLupa'])->name('password.email');;

    Route::get('/reset-password/{token}', [ResetPasswordController::class, 'create'])->name('password.reset');
    Route::post('/reset-password', [ResetPasswordController::class, 'store'])->name('password.update');
});

Route::get('/logout', [AuthController::class, 'logout']);

//LOGIN USER PAGES
Route::middleware('auth')->group(function () {

    //DASHBOARD
    Route::get('/dashboard', [DashboardController::class, 'index']);

    //BIODATA DAN UBAH PASSWORD
    Route::get('/biodata', [BiodataController::class, 'index']);
    Route::put('/edit-biodata/{id}', [BiodataController::class, 'update']);
    Route::put('/edit-pass/{id}', [BiodataController::class, 'updatePass']);

    //MANAJEMEN USER
    Route::middleware(['role:1,2'])->group(function () {
        Route::get('/manajemen-user', [UserController::class, 'index']);
        Route::post('/tambah-user', [UserController::class, 'store']);
        Route::put('/edit-user/{id}', [UserController::class, 'update']);
        Route::delete('/hapus-user/{id}', [UserController::class, 'destroy']);

        //DAFTAR PERMOHONAN
        Route::get('/koorprodi-permohonan', [PermohonanController::class, 'daftarkoor']);
        Route::get('/detail-koorprodi/{id}', [PermohonanController::class, 'koorprodi']);
    });

    //DAFTAR PERMOHONAN
    Route::get('/daftar-permohonan', [PermohonanController::class, 'daftar']);
    Route::get('/daftar-permohonan-koorprodi', [PermohonanController::class, 'daftarkoorprodi']);
    Route::get('/daftar-permohonan-koordosen', [PermohonanController::class, 'daftarkoordosen']);
    Route::get('/detail-permohonan/{id}', [PermohonanController::class, 'detail']);
    Route::get('/permohonan-selesai', [PermohonanController::class, 'daftarselesai']);
    Route::post('/permohonan-setuju/{id}', [PermohonanController::class, 'setuju']);
    Route::post('/admin-setuju/{id}', [PermohonanController::class, 'adminsetuju']);
    Route::post('/permohonan-tidak-setuju/{id}', [PermohonanController::class, 'tidaksetuju']);
    // Route::post('/bulk', [PermohonanController::class, 'bulk']);
});

//UNDUH DOKUMEN DAN QR CODE
Route::get('/download/{file}', [PermohonanController::class, 'download']);
Route::get('/download-qr-code/{file}', [PermohonanController::class, 'qrcode']);


//PUBLIK PAGES
Route::get('/', function () {
    return view('publik.landing-page');
});

//PERMOHONAN
Route::get('/permohonan', [PermohonanController::class, 'index']);
Route::post('/permohonan', [PermohonanController::class, 'store']);
Route::get('/permohonan-sukses/{uuid}', [PermohonanController::class, 'sukses']);

//PERMOHONAN KOORPRODI
Route::get('/permohonan-koorprodi', [PermohonanController::class, 'indexKoorprodi']);
Route::post('/permohonan-koorprodi', [PermohonanController::class, 'storeKoorprodi']);
// Route::get('/permohonan-sukses/{id}', [PermohonanController::class, 'sukses']);

//CEK STATUS
Route::get('/status', [StatusController::class, 'index']);
Route::post('/status', [StatusController::class, 'cek']);
Route::get('/hasil-cek/{token}', [StatusController::class, 'hasilCek']);

Route::get('/cara-pakai', function () {
    return view('publik.cara-pakai');
});

//FILL DATA
Route::post('/dokumen-selesai/{id}', [PermohonanController::class, 'final']);
Route::get('/download-dok-selesai/{id}', [PermohonanController::class, 'downloadFinal']);
Route::get('/fill-data/{id}', [PDFController::class, 'index']);

//INFORMASI TANDA TANGAN
Route::get('/tanda-tangan/{uuid}', [VerifikasiController::class, 'index']);
// Route::get('/download-file/{file}', [PermohonanController::class, 'download-file']);

//KIRIM EMAIL
Route::get('/email-token/{id}', [EmailController::class, 'tokenEmail']);
Route::get('/email-token-koorprodi/{id}', [EmailController::class, 'tokenEmailKoor']);
Route::get('/email-setuju/{id}', [EmailController::class, 'emailSetuju']);
Route::get('/email-admin-setuju/{id}', [EmailController::class, 'adminSetuju']);
Route::get('/email-ditolak/{id}', [EmailController::class, 'emailDitolak']);

//PANDUAN
Route::get('/panduan', [PanduanController::class, 'index']);


//CAPTCHA
Route::get('/reload-captcha', [AuthController::class, 'reloadCaptcha']);
