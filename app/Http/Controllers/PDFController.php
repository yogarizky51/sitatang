<?php

namespace App\Http\Controllers;

use App\Models\Dokumen;
use App\Models\Permohonan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use setasign\Fpdi\Fpdi;
use Spatie\PdfToImage\Pdf;
use Xthiago\PDFVersionConverter\Guesser\RegexGuesser;
// use Org_Heigl\Ghostscript\Ghostscript;
use Imagick;
use GravityMedia\Ghostscript\Ghostscript;
use Symfony\Component\Process\Process;

class PDFController extends Controller
{
    public function index($uuid)
    {
        // $permohonan = Permohonan::findorfail($id);
        $permohonan = Permohonan::where('uuid', $uuid)->first();
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->first();
        $filePath = public_path("file_surat/" . $dokumen->rename);
        $outputFilePath = public_path("file_selesai/" . "[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".pdf");

        chdir(public_path("file_surat/"));
        shell_exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -o New_" . $dokumen->rename . " " . $dokumen->rename . " ");
        $convert_file = public_path("file_surat/New_" . $dokumen->rename);
        // dd($convert_file);
        $this->fillPDF($convert_file, $outputFilePath);

        $pdf = new Pdf($outputFilePath);
        $pdf->saveImage(public_path("file_gambar/" . "[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".jpg"));

        return redirect('/permohonan-sukses/' . $uuid);

        // $image_path = public_path("file_gambar/" . "[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".jpg");
        // dd("done");

        // return response()->file($image_path);
        // return dd($outputAwal);
        // $path = public_path("file_surat/");
        // Define input and output files
        // $inputFile = $filePath;
        // $outputFile = $outputFilePath;

        // // Create Ghostscript object
        // $ghostscript = new Ghostscript([
        //     'quiet' => false
        // ]);

        // // Create and configure the device
        // $device = $ghostscript->createPdfDevice($outputFile);
        // $device->setCompatibilityLevel(1.4);

        // // Create process
        // $process = $device->createProcess($inputFile);

        // // Print the command line
        // print '$ ' . $process->getCommandLine() . PHP_EOL;

        // // Run process
        // $process->run(function ($type, $buffer) {
        //     if ($type === Process::ERR) {
        //         throw new \RuntimeException($buffer);
        //     }

        //     print $buffer;
        // });
        // $guesser = new RegexGuesser();
        // $versi =  $guesser->guess($filePath); // will print something like '1.4'
        // $file = $dokumen-
        // $outputAwal = shell_exec("gswin64c -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=" . "new_" . $filePath . " " . $filePath . " ");
        // $outputAwal = public_path("file_surat/" . $convert);
        // $filePath = public_path("file_surat/" . $dokumen->rename);
        // $outputFilePath = public_path("file_selesai/" . "[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".pdf");
        // $outputFileImage = public_path("file_selesai/" . "[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon);
        // $this->basicTable($header, $data);
        // dd($outputFile);
        // Ghostscript::setGsPath("C:\Program Files\gs\gs10.00.0\bin\gs.exe");
        // $imagick = new Imagick();

        // $imagick->readImage($outputFilePath);
        // // $imagick->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        // $imagick->setImageFormat("jpeg");
        // $imagick->writeImages("[PREVIEW]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".jpeg", true);

    }

    public function fillPDF($convert_file, $outputFilePath)
    {
        $fpdi = new FPDI('P', 'mm', [210, 297]);

        $count = $fpdi->setSourceFile($convert_file);

        for ($i = 1; $i <= $count; $i++) {
            $template = $fpdi->importPage($i);
        }
        $last = $template;
        $size = $fpdi->getTemplateSize($last);
        $fpdi->AddPage();
        $fpdi->useTemplate($last);
        $fpdi->SetFont('Arial', '', 14);

        // $fpdi->SetFont("helvetica", "", 15);
        // $fpdi->SetTextColor(153, 0, 153);

        // $left = 100;
        // $top = 10;
        // $text = "itsolutionstuff.com";
        // $fpdi->Text($left, $top, $text);

        $fpdi->Image(public_path("grid.png"), 0, 0, -300, -300);

        return $fpdi->Output('F', $outputFilePath);
    }
}
