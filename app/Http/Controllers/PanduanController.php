<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PanduanController extends Controller
{
    public function index()
    {

        $filePath = public_path("tata_cara_sitatang.pdf");

        return response()->download($filePath);
    }
}
