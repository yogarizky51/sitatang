<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Role;
use App\Models\RolePengguna;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class BiodataController extends Controller
{
    public function index()
    {
        $users = User::all();
        $biodata = Biodata::all();
        $role  = Role::all();
        $akses = RolePengguna::all();
        $data   = [
            'users' => $users,
            'role' => $role,
            'biodata' => $biodata,
            'akses' => $akses
        ];
        return view('admin.biodata', $data);
    }

    public function update(Request $request, $id)
    {
        $user = User::findorfail($id);
        // $biodata = Biodata::where('biodata_id', $user->biodata_id)->firstorfail();
        $validator = Validator::make(
            $request->all(),
            [
                'nama' => 'required', 'alpha_num',
                'email' => 'required',
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong',
                'email.required' => 'email tidak boleh kosong',
                'email.unique' => 'email sudah terdaftar',
            ]
        );
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        // $biodata = new Biodata();
        // $biodata->nip = $request->nip;
        // $biodata->save();
        // Biodata::create([
        //     'nip' => $request->nip
        // ]);
        // User::create([
        //     'nama' => $request->nama,
        //     'uuid' => Str::uuid()->toString(),
        //     'email' => $request->email,
        //     'biodata_id' => $biodata->id,
        //     'password' => Hash::make($request->password),
        // ]);
        // $user = new User();
        $user->nama = $request->nama;
        // $user->uuid = Str::uuid()->toString();
        $user->email = $request->email;
        // $user->role_id = $request->role_id;
        // $user->biodata_id = $biodata->biodata_id;
        // $user->password = Hash::make($request->password);

        $user->save();

        // RolePengguna::create([
        //     'aktif' => $request->aktif,
        //     'akses_id' => $request->role_id,
        //     'pengguna_id' => $user->pengguna_id
        // ]);

        // $role_pengguna->aktif = $request->aktif;
        // $role_pengguna->akses_id = $request->role_id;
        // $role_pengguna->pengguna_id = $user->pengguna_id;

        // $role_pengguna->save();

        // dd('sukses');
        return redirect('/biodata')->with('toast_success', 'Biodata Berhasil Diubah!');
    }

    public function updatePass(Request $request, $id)
    {
        // $user = User::findorfail($id);
        // $biodata = Biodata::where('biodata_id', $user->biodata_id)->firstorfail();
        $validator = Validator::make(
            $request->all(),
            [
                'password' => 'required', 'confirmed'
                // 'confirm-pass' => 'required',
            ],
            [
                'password.required' => 'Nama tidak boleh kosong',
                // 'confirm-pass.required' => 'email tidak boleh kosong',
            ]
        );
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        auth()->user()->update(['password' => Hash::make($request->password)]);
        return back()->with('toast_success', 'Password Berhasil Diubah!');
    }
}
