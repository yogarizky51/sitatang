<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Role;
use App\Models\RolePengguna;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $biodata = Biodata::all();
        $role  = Role::all();
        $akses = RolePengguna::all();
        $data   = [
            'users' => $users,
            'role' => $role,
            'biodata' => $biodata,
            'akses' => $akses
        ];
        return view('admin.manajemen-user', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'nama' => 'required', 'alpha_num',
                'email' => 'required|unique:pengguna,email',
                'password' => 'required',
                'nip' => 'required',
                'role_id_utama' => 'required',
                'role_id_kedua' => 'required',
                'aktif' => 'required',
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong',
                'email.required' => 'email tidak boleh kosong',
                'email.unique' => 'email sudah terdaftar',
                'password.required' => 'password tidak boleh kosong',
                'nip.required' => 'nip tidak boleh kosong',
                'role_id_utama.required' => 'Mohon pilih role utama',
                'role_id_kedua.required' => 'Mohon pilih role kedua',
                'aktif.required' => 'Mohon pilih status akun user',
            ]
        );
        if (!$validator->fails()) {
            if ($request->role_id_utama == 3 && $request->aktif == 1) {

                $checkaktif = RolePengguna::where('akses_id', 3)->where('aktif', 1)->doesntExist();
                if ($checkaktif) {
                    $biodata = new Biodata();
                    $biodata->nip = $request->nip;

                    $biodata->save();
                    // Biodata::create([
                    //     'nip' => $request->nip
                    // ]);
                    // User::create([
                    //     'nama' => $request->nama,
                    //     'uuid' => Str::uuid()->toString(),
                    //     'email' => $request->email,
                    //     'biodata_id' => $biodata->id,
                    //     'password' => Hash::make($request->password),
                    // ]);

                    $user = new User();
                    $user->nama = $request->nama;
                    $user->uuid = Str::uuid()->toString();
                    $user->email = $request->email;
                    $user->biodata_id = $biodata->biodata_id;
                    $user->role_id_utama = $request->role_id_utama;
                    $user->role_id_kedua = $request->role_id_kedua;
                    $user->password = Hash::make($request->password);
                    $user->save();

                    RolePengguna::create([
                        'aktif' => $request->aktif,
                        'akses_id' => $request->role_id_utama,
                        'pengguna_id' => $user->pengguna_id
                    ]);

                    // dd('sukses');
                    return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Ditambah!');
                } else {
                    return back()->with('toast_error', 'Sudah ada akun koorprodi yang aktif !');
                }
            } else {
                if ($request->role_id_utama == $request->role_id_kedua) {
                    return back()->with('toast_error', 'Jabatan utama dan merangkap tidak boleh sama');
                }
                $biodata = new Biodata();
                $biodata->nip = $request->nip;

                $biodata->save();
                // Biodata::create([
                //     'nip' => $request->nip
                // ]);
                // User::create([
                //     'nama' => $request->nama,
                //     'uuid' => Str::uuid()->toString(),
                //     'email' => $request->email,
                //     'biodata_id' => $biodata->id,
                //     'password' => Hash::make($request->password),
                // ]);

                $user = new User();
                $user->nama = $request->nama;
                $user->uuid = Str::uuid()->toString();
                $user->email = $request->email;
                $user->biodata_id = $biodata->biodata_id;
                $user->role_id_utama = $request->role_id_utama;
                $user->role_id_kedua = $request->role_id_kedua;
                $user->password = Hash::make($request->password);
                $user->save();

                RolePengguna::create([
                    'aktif' => $request->aktif,
                    'akses_id' => $request->role_id_utama,
                    'pengguna_id' => $user->pengguna_id
                ]);

                // dd('sukses');
                return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Ditambah!');
            }
        } else {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::findorfail($id);
        $biodata = Biodata::where('biodata_id', $user->biodata_id)->firstorfail();
        $role_pengguna = RolePengguna::where('pengguna_id', $user->pengguna_id)->firstorfail();
        $validator = Validator::make(
            $request->all(),
            [
                'nama' => 'required', 'alpha_num',
                'email' => 'required',
                'nip' => 'required',
                'role_id_utama' => 'required',
                'role_id_kedua' => 'required',
                'aktif' => 'required',
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong',
                'email.required' => 'email tidak boleh kosong',
                'email.unique' => 'email sudah terdaftar',
                'nip.required' => 'nip tidak boleh kosong',
                'role_id_utama.required' => 'Mohon pilih role utama',
                'role_id_kedua.required' => 'Mohon pilih role kedua',
                'status.required' => 'Mohon pilih status akun user',
            ]
        );
        if (!$validator->fails()) {
            if ($request->role_id_utama == 3 && $request->aktif == 1) {
                $checkaktif = RolePengguna::where('akses_id', 3)->where('aktif', 1)->doesntExist();
                if ($checkaktif) {

                    // $biodata = new Biodata();
                    $biodata->nip = $request->nip;
                    $biodata->save();
                    // Biodata::create([
                    //     'nip' => $request->nip
                    // ]);
                    // User::create([
                    //     'nama' => $request->nama,
                    //     'uuid' => Str::uuid()->toString(),
                    //     'email' => $request->email,
                    //     'biodata_id' => $biodata->id,
                    //     'password' => Hash::make($request->password),
                    // ]);

                    // $user = new User();
                    $user->nama = $request->nama;
                    $user->uuid = Str::uuid()->toString();
                    $user->email = $request->email;
                    $user->role_id_utama = $request->role_id_utama;
                    $user->role_id_kedua = $request->role_id_kedua;
                    $user->biodata_id = $biodata->biodata_id;
                    // $user->password = Hash::make($request->password);

                    $user->save();

                    // RolePengguna::create([
                    //     'aktif' => $request->aktif,
                    //     'akses_id' => $request->role_id,
                    //     'pengguna_id' => $user->pengguna_id
                    // ]);

                    $role_pengguna->aktif = $request->aktif;
                    $role_pengguna->akses_id = $request->role_id_utama;
                    $role_pengguna->pengguna_id = $user->pengguna_id;

                    $role_pengguna->save();

                    // dd('sukses');
                    return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Diubah!');
                } elseif ($request->role_id_utama == $user->role_id_utama && $request->aktif == $role_pengguna->aktif) {
                    // $biodata = new Biodata();
                    $biodata->nip = $request->nip;
                    $biodata->save();
                    // Biodata::create([
                    //     'nip' => $request->nip
                    // ]);
                    // User::create([
                    //     'nama' => $request->nama,
                    //     'uuid' => Str::uuid()->toString(),
                    //     'email' => $request->email,
                    //     'biodata_id' => $biodata->id,
                    //     'password' => Hash::make($request->password),
                    // ]);

                    // $user = new User();
                    $user->nama = $request->nama;
                    $user->uuid = Str::uuid()->toString();
                    $user->email = $request->email;
                    $user->role_id_utama = $request->role_id_utama;
                    $user->role_id_kedua = $request->role_id_kedua;
                    $user->biodata_id = $biodata->biodata_id;
                    // $user->password = Hash::make($request->password);

                    $user->save();

                    // RolePengguna::create([
                    //     'aktif' => $request->aktif,
                    //     'akses_id' => $request->role_id,
                    //     'pengguna_id' => $user->pengguna_id
                    // ]);

                    $role_pengguna->aktif = $request->aktif;
                    $role_pengguna->akses_id = $request->role_id_utama;
                    $role_pengguna->pengguna_id = $user->pengguna_id;

                    $role_pengguna->save();

                    // dd('sukses');
                    return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Diubah!');
                } else {
                    return back()->with('toast_error', 'Sudah ada akun koorprodi yang aktif !');
                }
            } else {
                if ($request->role_id_utama == $request->role_id_kedua) {
                    return back()->with('toast_error', 'Jabatan utama dan merangkap tidak boleh sama');
                }
                // $biodata = new Biodata();
                $biodata->nip = $request->nip;
                $biodata->save();
                // Biodata::create([
                //     'nip' => $request->nip
                // ]);
                // User::create([
                //     'nama' => $request->nama,
                //     'uuid' => Str::uuid()->toString(),
                //     'email' => $request->email,
                //     'biodata_id' => $biodata->id,
                //     'password' => Hash::make($request->password),
                // ]);
                // $user = new User();
                $user->nama = $request->nama;
                $user->uuid = Str::uuid()->toString();
                $user->email = $request->email;
                $user->role_id_utama = $request->role_id_utama;
                if ($request->role_id_kedua != null) {
                    $user->role_id_kedua = $request->role_id_kedua;
                } else {
                    $user->role_id_kedua = null;
                }
                $user->biodata_id = $biodata->biodata_id;
                // $user->password = Hash::make($request->password);
                $user->save();

                // RolePengguna::create([
                //     'aktif' => $request->aktif,
                //     'akses_id' => $request->role_id,
                //     'pengguna_id' => $user->pengguna_id
                // ]);

                $role_pengguna->aktif = $request->aktif;
                $role_pengguna->akses_id = $request->role_id_utama;
                $role_pengguna->pengguna_id = $user->pengguna_id;
                $role_pengguna->save();

                // dd('sukses');
                return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Diubah!');
            }
        } else {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $users = User::find($id);
        $biodata = Biodata::where('biodata_id', $users->biodata_id);
        $role_pengguna = RolePengguna::where('pengguna_id', $users->pengguna_id);
        $biodata->delete();
        $role_pengguna->delete();
        $users->delete();

        return redirect('/manajemen-user')->with('toast_success', 'Pengguna Berhasil Dihapus!');
    }

    public function indexLupa()
    {
        return view('auth.lupa-pass');
    }

    public function storeLupa(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);
        $status = Password::sendResetLink(
            $request->only('email')
        );
        if ($status == Password::RESET_LINK_SENT) {
            return back()->with('status', __($status));
        }
        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }
}
