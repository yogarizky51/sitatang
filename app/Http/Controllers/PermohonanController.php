<?php

namespace App\Http\Controllers;

use App\Models\Dokumen;
use App\Models\Permohonan;
use App\Models\Posisi;
use App\Models\RolePengguna;
use App\Models\User;
use App\Notifications\StatusNotification;
use App\Notifications\TokenNotification;
use Illuminate\Http\Request;
use Notification;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use setasign\Fpdi\Fpdi;
use Spatie\PdfToImage\Pdf;


class PermohonanController extends Controller
{
    public function index()
    {
        $users = User::all();
        $rp = RolePengguna::all();
        $data   = [
            'users' => $users,
            'rp' => $rp,
        ];
        return view('publik.pengajuan', $data);
    }

    public function indexKoorprodi()
    {
        return view('publik.pengajuan-koorprodi');
    }

    public function sukses($uuid)
    {
        $uuid = Permohonan::where('uuid', $uuid)->first();
        // $pemohon = Permohonan::findorfail($id);
        $pemohon = Permohonan::where('permohonan_id', $uuid->permohonan_id)->first();
        $data   = [
            'pemohon' => $pemohon
        ];
        return view('publik.permohonan-sukses', $data);
    }

    public function store(Request $request)
    {

        $request->validate(
            [
                'nama' => 'required',
                'email' => 'required',
                'pengguna_id' => 'required',
                'pesan' => 'required',
                'file1' => 'required|mimes:pdf',
                'file2' => 'mimes:pdf'
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong',
                'email.required' => 'email tidak boleh kosong',
                'pengguna_id.required' => 'Mohon isi diajukan kepada siapa',
                'pesan.required' => 'Pesan tidak boleh kosong',
                'file1.required' => 'File tidak boleh kosong',
                'file1.mimes' => 'File bukan PDF',
                'file2.mimes' => 'File bukan PDF',
            ]
        );

        if ($request->hasFile('file1')) {
            $uploadPath1 = public_path('file_surat');

            if (!File::isDirectory($uploadPath1)) {
                File::makeDirectory($uploadPath1, 0755, true, true);
            }

            $file1 = $request->file('file1');
            $explode1 = explode('.', $file1->getClientOriginalName());
            $originalName1 = $explode1[0];
            $split1 = explode(" ", $originalName1);
            $join = join("_", $split1);
            $extension1 = $file1->getClientOriginalExtension();
            $rename1 = '[PERMOHONAN]_' . $join . '-' . time() . '.' . $extension1;
            // $rename = '[PERMOHONAN]' . date('YmdHis') . '.' . $extension;
            $mime1 = $file1->getClientMimeType();
            $filesize1 = $file1->getSize();

            if ($file1->move($uploadPath1, $rename1)) {

                if ($request->hasFile('file2')) {
                    $uploadPath2 = public_path('file_surat');

                    if (!File::isDirectory($uploadPath1)) {
                        File::makeDirectory($uploadPath1, 0755, true, true);
                    }

                    $file2 = $request->file('file2');
                    $explode2 = explode('.', $file2->getClientOriginalName());
                    $originalName2 = $explode2[0];
                    $extension2 = $file2->getClientOriginalExtension();
                    $rename2 = '[ATTACHMENT]_' . $originalName2 . '-' . time() . '.' . $extension2;
                    // $rename = '[PERMOHONAN]' . date('YmdHis') . '.' . $extension;
                    $mime2 = $file2->getClientMimeType();
                    $filesize2 = $file2->getSize();

                    if ($file2->move($uploadPath2, $rename2)) {
                        $role = User::where('pengguna_id', $request->pengguna_id)->first();
                        // $koor_id = User::where('role_id', 3)->first()->pengguna_id;
                        // $admin_id = User::where('role_id', 2)->first()->pengguna_id;
                        $pemohon = new Permohonan();
                        $pemohon->uuid = Str::uuid()->toString();
                        $pemohon->status_id = 1;
                        $pemohon->pengguna_id = $request->pengguna_id;
                        if ($role->role_id_utama == 3) {
                            $pemohon->role_id = $role->role_id_kedua;
                        } else {
                            $pemohon->role_id = $role->role_id_utama;
                        }
                        $pemohon->nama_pemohon = $request->nama;
                        $pemohon->email_pemohon = $request->email;
                        $pemohon->judul_surat = $originalName1;
                        $pemohon->judul_attachment = $originalName2;
                        $pemohon->token_pemohon = Str::random(5) . '_' . time();
                        $pemohon->pesan = $request->pesan;
                        $pemohon->save();

                        $dokumen1 = new Dokumen();
                        $dokumen1->uuid = Str::uuid()->toString();
                        $dokumen1->permohonan_id = $pemohon->permohonan_id;
                        $dokumen1->jenisdokumen_id = 1;
                        $dokumen1->file_name = $originalName1;
                        $dokumen1->rename = $rename1;
                        $dokumen1->extension = $extension1;
                        $dokumen1->file_size = $filesize1;
                        $dokumen1->mime_type = $mime1;
                        $dokumen1->path = $uploadPath1;
                        $dokumen1->save();

                        $dokumen2 = new Dokumen();
                        $dokumen2->uuid = Str::uuid()->toString();
                        $dokumen2->permohonan_id = $pemohon->permohonan_id;
                        $dokumen2->jenisdokumen_id = 4;
                        $dokumen2->file_name = $originalName2;
                        $dokumen2->rename = $rename2;
                        $dokumen2->extension = $extension2;
                        $dokumen2->file_size = $filesize2;
                        $dokumen2->mime_type = $mime2;
                        $dokumen2->path = $uploadPath2;
                        $dokumen2->save();


                        // dd($role);
                        return redirect('/email-token/' . $pemohon->uuid);
                    }
                    return redirect()->back()->with('toast_error', 'Error, file attachment tidak dapat di upload');
                }
                $role = User::where('pengguna_id', $request->pengguna_id)->first();

                $pemohon = new Permohonan();
                $pemohon->uuid = Str::uuid()->toString();
                $pemohon->status_id = 1;
                $pemohon->pengguna_id = $request->pengguna_id;
                if ($role->role_id_utama == 3) {
                    $pemohon->role_id = $role->role_id_kedua;
                } else {
                    $pemohon->role_id = $role->role_id_utama;
                }
                $pemohon->nama_pemohon = $request->nama;
                $pemohon->email_pemohon = $request->email;
                $pemohon->judul_surat = $originalName1;
                $pemohon->token_pemohon = Str::random(5) . '_' . time();
                $pemohon->pesan = $request->pesan;
                $pemohon->save();

                $dokumen1 = new Dokumen();
                $dokumen1->uuid = Str::uuid()->toString();
                $dokumen1->permohonan_id = $pemohon->permohonan_id;
                $dokumen1->jenisdokumen_id = 1;
                $dokumen1->file_name = $originalName1;
                $dokumen1->rename = $rename1;
                $dokumen1->extension = $extension1;
                $dokumen1->file_size = $filesize1;
                $dokumen1->mime_type = $mime1;
                $dokumen1->path = $uploadPath1;
                $dokumen1->save();

                // Notification::send($pemohon, new TokenNotification($data_email));

                return redirect('/email-token/' . $pemohon->uuid);
            }
            return redirect()->back()->with('toast_error', 'Error, file tidak dapat di upload');
        }

        return redirect()->back()->with('toast_error', 'Error, tidak ada file ditemukan');
    }

    public function storeKoorprodi(Request $request)
    {

        $request->validate(
            [
                'nama' => 'required',
                'email' => 'required',
                'pesan' => 'required',
                'file1' => 'required|mimes:pdf',
                'file2' => 'mimes:pdf'
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong',
                'email.required' => 'email tidak boleh kosong',
                'pesan.required' => 'Pesan tidak boleh kosong',
                'file1.required' => 'File tidak boleh kosong',
                'file1.mimes' => 'File bukan PDF',
                'file2.mimes' => 'File bukan PDF',
            ]
        );

        if ($request->hasFile('file1')) {
            $uploadPath1 = public_path('file_surat');

            if (!File::isDirectory($uploadPath1)) {
                File::makeDirectory($uploadPath1, 0755, true, true);
            }

            $file1 = $request->file('file1');
            $explode1 = explode('.', $file1->getClientOriginalName());
            $originalName1 = $explode1[0];
            $split1 = explode(" ", $originalName1);
            $join = join("_", $split1);
            $extension1 = $file1->getClientOriginalExtension();
            $rename1 = '[PERMOHONAN]_' . $join . '-' . time() . '.' . $extension1;
            // $rename = '[PERMOHONAN]' . date('YmdHis') . '.' . $extension;
            $mime1 = $file1->getClientMimeType();
            $filesize1 = $file1->getSize();

            if ($file1->move($uploadPath1, $rename1)) {

                if ($request->hasFile('file2')) {
                    $uploadPath2 = public_path('file_surat');

                    if (!File::isDirectory($uploadPath1)) {
                        File::makeDirectory($uploadPath1, 0755, true, true);
                    }

                    $file2 = $request->file('file2');
                    $explode2 = explode('.', $file2->getClientOriginalName());
                    $originalName2 = $explode2[0];
                    $extension2 = $file2->getClientOriginalExtension();
                    $rename2 = '[ATTACHMENT]_' . $originalName2 . '-' . time() . '.' . $extension2;
                    // $rename = '[PERMOHONAN]' . date('YmdHis') . '.' . $extension;
                    $mime2 = $file2->getClientMimeType();
                    $filesize2 = $file2->getSize();

                    if ($file2->move($uploadPath2, $rename2)) {
                        $koorprodi = User::where('role_id_utama', 3)->first();
                        // $koor_id = User::where('role_id', 3)->first()->pengguna_id;
                        // $admin_id = User::where('role_id', 2)->first()->pengguna_id;
                        $pemohon = new Permohonan();
                        $pemohon->uuid = Str::uuid()->toString();
                        $pemohon->status_id = 1;
                        $pemohon->pengguna_id = $koorprodi->pengguna_id;
                        $pemohon->role_id = 3;
                        $pemohon->nama_pemohon = $request->nama;
                        $pemohon->email_pemohon = $request->email;
                        $pemohon->judul_surat = $originalName1;
                        $pemohon->judul_attachment = $originalName2;
                        $pemohon->token_pemohon = Str::random(5) . '_' . time();
                        $pemohon->pesan = $request->pesan;
                        $pemohon->save();

                        $dokumen1 = new Dokumen();
                        $dokumen1->uuid = Str::uuid()->toString();
                        $dokumen1->permohonan_id = $pemohon->permohonan_id;
                        $dokumen1->jenisdokumen_id = 1;
                        $dokumen1->file_name = $originalName1;
                        $dokumen1->rename = $rename1;
                        $dokumen1->extension = $extension1;
                        $dokumen1->file_size = $filesize1;
                        $dokumen1->mime_type = $mime1;
                        $dokumen1->path = $uploadPath1;
                        $dokumen1->save();

                        $dokumen2 = new Dokumen();
                        $dokumen2->uuid = Str::uuid()->toString();
                        $dokumen2->permohonan_id = $pemohon->permohonan_id;
                        $dokumen2->jenisdokumen_id = 4;
                        $dokumen2->file_name = $originalName2;
                        $dokumen2->rename = $rename2;
                        $dokumen2->extension = $extension2;
                        $dokumen2->file_size = $filesize2;
                        $dokumen2->mime_type = $mime2;
                        $dokumen2->path = $uploadPath2;
                        $dokumen2->save();

                        // Notification::send($pemohon, new TokenNotification($data_email));

                        // dd($koorprodi->pengguna_id);
                        return redirect('/email-token-koorprodi/' . $pemohon->uuid);
                    }
                    return redirect()->back()->with('toast_error', 'Error, file attachment tidak dapat di upload');
                }
                $koorprodi = User::where('role_id_utama', 3)->first();
                // $koor_id = User::where('role_id', 3)->first()->pengguna_id;
                // $admin_id = User::where('role_id', 2)->first()->pengguna_id;
                $pemohon = new Permohonan();
                $pemohon->uuid = Str::uuid()->toString();
                $pemohon->status_id = 1;
                $pemohon->pengguna_id = $koorprodi->pengguna_id;
                $pemohon->role_id = 3;
                $pemohon->nama_pemohon = $request->nama;
                $pemohon->email_pemohon = $request->email;
                $pemohon->judul_surat = $originalName1;
                $pemohon->token_pemohon = Str::random(5) . '_' . time();
                $pemohon->pesan = $request->pesan;
                $pemohon->save();

                $dokumen1 = new Dokumen();
                $dokumen1->uuid = Str::uuid()->toString();
                $dokumen1->permohonan_id = $pemohon->permohonan_id;
                $dokumen1->jenisdokumen_id = 1;
                $dokumen1->file_name = $originalName1;
                $dokumen1->rename = $rename1;
                $dokumen1->extension = $extension1;
                $dokumen1->file_size = $filesize1;
                $dokumen1->mime_type = $mime1;
                $dokumen1->path = $uploadPath1;
                $dokumen1->save();

                // Notification::send($pemohon, new TokenNotification($data_email));

                return redirect('/email-token-koorprodi/' . $pemohon->uuid);
            }
            return redirect()->back()->with('toast_error', 'Error, file tidak dapat di upload');
        }

        return redirect()->back()->with('toast_error', 'Error, tidak ada file ditemukan');
    }

    public function setuju($id)
    {

        $permohonan = Permohonan::findorfail($id);
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->first();
        $link_qr = 'tanda-tangan/' . $dokumen->uuid;
        $qr_encode = Hash::make(Qrcode::format('png')->size(300)->merge(public_path('logo.png'), .2, true)->generate(url($link_qr)));
        $qrcode = Qrcode::format('png')->size(300)->merge(public_path('logo.png'), .2, true)->generate(url($link_qr));

        $qr_name = 'img-' . $permohonan->judul_surat . '-' . time() . '.png';
        $qrcode_file = '/qr-code/' . $qr_name;
        Storage::disk('public')->put($qrcode_file, $qrcode);

        $form = [
            'status_id' => 2,
            'qr_code' => $qr_encode,
            'qr_name' => $qr_name
        ];

        Permohonan::where('permohonan_id', $id)->update($form);

        return redirect('/email-setuju/' . $permohonan->permohonan_id);
        // return redirect('/dokumen-selesai/' . $id);
    }

    public function final(Request $request, $id)
    {

        $permohonan = Permohonan::findorfail($id);
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->first();
        $request->validate(
            [
                'posisi_id' => 'required',
            ],
            [
                'posisi_id.required' => 'Mohon isi posisi tanda tangan',
            ]
        );

        $form = [
            'posisi_id' => $request->posisi_id
        ];

        Permohonan::where('permohonan_id', $id)->update($form);

        return redirect('/download-dok-selesai/' . $permohonan->permohonan_id);
    }

    public function downloadFinal($id)
    {
        $permohonan = Permohonan::findorfail($id);
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->first();
        $filePath = public_path("file_surat/New_" . $dokumen->rename);
        $outputFilePath = public_path("file_selesai/" . "[DISETUJUI]_" . $permohonan->judul_surat . $permohonan->token_pemohon . ".pdf");
        $this->fillpdf($filePath, $outputFilePath, $permohonan);

        return response()->download($outputFilePath);

        // PENGUBAH VERSI PDF
        // chdir(public_path("file_surat/"));
        // shell_exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -o New_" . $dokumen->rename . " " . $dokumen->rename . " ");
        // $convert_file = public_path("file_surat/New_" . $dokumen->rename);
    }

    public function adminsetuju($id)
    {
        $users = User::where('role_id_utama', 3)->first()->pengguna_id;
        $form = [
            'status_id' => 4,
            'pengguna_id' => $users
        ];

        Permohonan::where('permohonan_id', $id)->update($form);

        return redirect('/email-admin-setuju/' . $id);
    }

    public function tidaksetuju(Request $request, $id)
    {
        $permohonan = Permohonan::findorfail($id);
        $form = [
            'status_id' => 3,
            'note' => $request->note
        ];

        Permohonan::where('permohonan_id', $id)->update($form);

        return redirect('/email-ditolak/' . $permohonan->permohonan_id);
    }

    public function daftar()
    {

        $permohonan = Permohonan::all();
        $data   = [
            'permohonan' => $permohonan
        ];
        return view('admin.daftar-permohonan', $data);
    }

    public function daftarKoorprodi()
    {

        $permohonan = Permohonan::all();
        $data   = [
            'permohonan' => $permohonan
        ];
        return view('admin.daftar-koorprodi', $data);
    }

    public function daftarKoordosen()
    {

        $permohonan = Permohonan::all();
        $data   = [
            'permohonan' => $permohonan
        ];
        return view('admin.daftar-koordosen', $data);
    }

    public function daftarselesai()
    {

        $permohonan = Permohonan::all();
        $data   = [
            'permohonan' => $permohonan
        ];
        return view('admin.daftar-selesai', $data);
    }

    public function qrcode($file_name)
    {
        $filePath       = public_path("storage/qr-code/") . $file_name;
        return response()->download($filePath);
    }

    public function detail($id)
    {

        $permohonan = Permohonan::findorfail($id);
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->firstorfail();
        $attach = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->where('jenisdokumen_id', 4)->first();
        // dd($detail);
        return view('admin.detail-permohonan', compact('permohonan', 'dokumen', 'attach'));
    }

    public function daftarKoor()
    {

        $permohonan = Permohonan::all();
        $data   = [
            'permohonan' => $permohonan
        ];
        return view('admin.permohonan-koorprodi', $data);
    }

    public function koorprodi($id)
    {

        $permohonan = Permohonan::findorfail($id);
        $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->firstorfail();
        $attach = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->where('jenisdokumen_id', 4)->first();
        // dd($detail);
        return view('admin.detail-koorprodi', compact('permohonan', 'dokumen', 'attach'));
    }

    public function download($file_name)
    {

        // $headers = ['Content-Type: application/pdf'];
        // $fileName = time().'.pdf';
        // $jenis_dokumen  = $dokumen->jenisDokumen()->get('nama');
        // $request_name   = $dokumen->rename;
        // $nama_file      = $dokumen->filename;
        // $namafile       = $dokumen->filename;
        $filePath       = public_path("file_surat/") . $file_name;

        // file_20230207200101.pdf
        // dd($filePath);
        return response()->download($filePath);

        // $file_contents  = base64_decode($document->doc_content);
        // return response($file_contents)
        //     ->header('Cache-Control', 'no-cache private')
        //     ->header('Content-Description', 'File Transfer')
        //     ->header('Content-Type', $document->doc_mime)
        //     ->header('Content-length', strlen($file_contents))
        //     ->header('Content-Disposition', 'attachment; filename=' . $filename)
        //     ->header('Content-Transfer-Encoding', 'binary');
    }

    public function fillpdf($filePath, $outputFilePath, $permohonan)
    {

        $fpdi = new FPDI;

        $fpdi->AddFont('Times New Roman', '', 'times new roman.php'); //Regular

        $count = $fpdi->setSourceFile($filePath);

        for ($i = 1; $i <= $count; $i++) {

            $template = $fpdi->importPage($i);
            $size = $fpdi->getTemplateSize($template);
            $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
            $fpdi->useTemplate($template);
            if ($i == $count) {
                // $last = count($template - 1);

                $fpdi->SetFont("times new roman", "", 8);

                $widthawal = 5.5;
                $width = 5.5;
                $height = 20.9;
                $posisi = $permohonan->posisi_id;
                $posisiheight = $permohonan->posisi_id;
                $posisiwidth = $permohonan->posisi_id;

                for ($j = 0; $j <= $posisiheight; $j++) {
                    if ($posisiheight > 10) {
                        $posisiheight = $posisiheight - 10;
                        $height = $height + 20.2;
                    }
                }
                if ($posisiwidth <= 10) {
                    for ($k = 1; $k < $posisiwidth; $k++) {
                        $width = $width + 20;
                    }
                } else {
                    $increment = 10;
                    $width = 5.5;
                    // $posisiwidth = $posisiwidth - $increment;
                    // if ($posisiwidth > 10) {
                    //     $posisiwidth = 1;
                    // }
                    for ($i = 1; $i < $posisiwidth; $i++) {
                        if ($posisiwidth > 10) {
                            $posisiwidth = $posisiwidth - 10;
                        }
                    }
                    for ($k = 1; $k < $posisiwidth; $k++) {
                        if ($posisiwidth !== 1) {
                            $width = $width + 20;
                        }
                    }
                }
                $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                // dd($width, $posisiwidth);

                // switch ($posisi) {
                //     case $posisi <= 10:
                //         for ($i = 1; $i < $posisi; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     case $posisi >= 11 && $posisi <= 20:
                //         $height = $height + 20;
                //         for ($i = 1; $i < $posisi - 10; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     case $posisi >= 21 && $posisi <= 30:
                //         $height = $height + 40;
                //         for ($i = 1; $i < $posisi - 20; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     case $posisi >= 31 && $posisi <= 40:
                //         $height = $height + 60.2;
                //         for ($i = 1; $i < $posisi - 30; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     case $posisi >= 41 && $posisi <= 50:
                //         $height = $height + 80.2;
                //         for ($i = 1; $i < $posisi - 40; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     case $posisi >= 51 && $posisi <= 60:
                //         $height = $height + 101;
                //         for ($i = 1; $i < $posisi - 50; $i++) {
                //             $width = $width + 20;
                //         }
                //         $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), $width, $height, -405);
                //         break;
                //     default:
                //         # code...
                //         break;
                // }

                // if ($permohonan->posisi_id == 1) {
                //     // $left1 = 21;
                //     // $top1 = 29;
                //     // $tanggal = $permohonan->updated_at->translatedFormat('d F Y');
                //     // $fpdi->Text($left1, $top1, $tanggal);

                //     // $left2 = 21;
                //     // $top2 = 32;
                //     // $nama = $permohonan->pengguna->nama;
                //     // $fpdi->Text($left2, $top2, $nama);

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 2) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 25.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 3) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 45.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 4) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 5) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 85.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 6) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 105.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 7) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 8) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 145.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 9) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 165.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 10) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 11) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 12) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 13) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5.5, 20.9, -405);
                // } elseif ($permohonan->posisi_id == 14) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 131, 60, -500);
                // } elseif ($permohonan->posisi_id == 15) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 173, 60, -500);
                // } elseif ($permohonan->posisi_id == 16) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 79, -500);
                // } elseif ($permohonan->posisi_id == 17) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 47, 79, -500);
                // } elseif ($permohonan->posisi_id == 18) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 89, 79, -500);
                // } elseif ($permohonan->posisi_id == 19) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 131, 79, -500);
                // } elseif ($permohonan->posisi_id == 20) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 173, 79, -500);
                // } elseif ($permohonan->posisi_id == 21) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 173, 79, -500);
                // } elseif ($permohonan->posisi_id == 21) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 87, -500);
                // } elseif ($permohonan->posisi_id == 22) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 118, -500);
                // } elseif ($permohonan->posisi_id == 23) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 35, 118, -500);
                // } elseif ($permohonan->posisi_id == 24) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65, 118, -500);
                // } elseif ($permohonan->posisi_id == 25) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 95, 118, -500);
                // } elseif ($permohonan->posisi_id == 26) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125, 118, -500);
                // } elseif ($permohonan->posisi_id == 27) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 155, 118, -500);
                // } elseif ($permohonan->posisi_id == 28) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 118, -500);
                // } elseif ($permohonan->posisi_id == 29) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 149, -500);
                // } elseif ($permohonan->posisi_id == 30) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 35, 149, -500);
                // } elseif ($permohonan->posisi_id == 31) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65, 149, -500);
                // } elseif ($permohonan->posisi_id == 32) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 95, 149, -500);
                // } elseif ($permohonan->posisi_id == 33) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125, 149, -500);
                // } elseif ($permohonan->posisi_id == 34) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 155, 149, -500);
                // } elseif ($permohonan->posisi_id == 35) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 149, -500);
                // } elseif ($permohonan->posisi_id == 36) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 180, -500);
                // } elseif ($permohonan->posisi_id == 37) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 35, 180, -500);
                // } elseif ($permohonan->posisi_id == 38) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65, 180, -500);
                // } elseif ($permohonan->posisi_id == 39) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 95, 180, -500);
                // } elseif ($permohonan->posisi_id == 40) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125, 180, -500);
                // } elseif ($permohonan->posisi_id == 41) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 155, 180, -500);
                // } elseif ($permohonan->posisi_id == 42) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 180, -500);
                // } elseif ($permohonan->posisi_id == 43) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 211, -500);
                // } elseif ($permohonan->posisi_id == 44) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 35, 211, -500);
                // } elseif ($permohonan->posisi_id == 45) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65, 211, -500);
                // } elseif ($permohonan->posisi_id == 46) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 95, 211, -500);
                // } elseif ($permohonan->posisi_id == 47) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125, 211, -500);
                // } elseif ($permohonan->posisi_id == 48) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 155, 211, -500);
                // } elseif ($permohonan->posisi_id == 49) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 211, -500);
                // } elseif ($permohonan->posisi_id == 50) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 5, 242, -500);
                // } elseif ($permohonan->posisi_id == 51) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 35, 242, -500);
                // } elseif ($permohonan->posisi_id == 52) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 65, 242, -500);
                // } elseif ($permohonan->posisi_id == 53) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 95, 242, -500);
                // } elseif ($permohonan->posisi_id == 54) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 125, 242, -500);
                // } elseif ($permohonan->posisi_id == 55) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 155, 242, -500);
                // } elseif ($permohonan->posisi_id == 56) {

                //     $fpdi->Image(public_path("storage/qr-code/" . $permohonan->qr_name), 185, 242, -500);
                // }
            }
        }

        return $fpdi->Output('F', $outputFilePath);
    }
}



// public function bulk(Request $request, $bulk)
    // {
    //     $validator = Validator::make(
    //         $request->all(
    //             [
    //                 'bulk' => 'required',
    //             ],
    //             [
    //                 'bulk.required' => 'Mohon ceklis pengajuan yang ingin diproses',
    //             ]
    //         )
    //     );

    //     if ($validator->fails()) {
    //         return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
    //     }

    //     $bulk = $request->input('bulk');

    //     switch ($request->input('semua')) {
    //         case 'setuju':
    //             foreach ($bulk as $b) {
    //                 $permohonan = Permohonan::findorfail($b);
    //                 $dokumen = Dokumen::where('permohonan_id', $permohonan->permohonan_id)->first();
    //                 $link_qr = 'tanda-tangan/' . $dokumen->uuid;
    //                 $qr_encode = base64_encode(Qrcode::format('png')->size(300)->merge(public_path('logo.png'), .2, true)->generate(url($link_qr)));
    //                 $qrcode = Qrcode::format('png')->size(300)->merge(public_path('logo.png'), .2, true)->generate(url($link_qr));

    //                 $qrcode_file = '/qr-code/img-' . time() . '.png';
    //                 Storage::disk('public')->put($qrcode_file, $qrcode);

    //                 $form = [
    //                     'status_id' => 2,
    //                     'qr_code' => $qr_encode
    //                 ];

    //                 Permohonan::where('permohonan_id', $b)->update($form);

    //                 return redirect('/daftar-permohonan')->with('toast_success', 'Pengajuan berhasil disetujui');
    //                 break;
    //             }
    //         case 'tidaksetuju':
    //             # code...
    //             break;
    //     }
    // }