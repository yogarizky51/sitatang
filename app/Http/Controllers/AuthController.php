<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\RolePengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {

        $request->validate(
            [
                'email' => 'required',
                'password' => 'required',
                'aktif' => '1',
                'captcha' => ['required', 'captcha'],
            ]
        );

        $remember = $request->remember ? true : false;

        // $email = $request->email;
        // $password = $request->password;

        // dd('berhasil login');
        if (Auth::attempt($request->only('email', 'password'), $remember)) {
            $request->session()->regenerate();
            // dd($credentials);
            // return redirect()->intended('dashboard');
            // dd(session()->all());
            return redirect('dashboard');
        }

        throw ValidationException::withMessages([
            'email' => 'Email salah atau tidak ditemukan.',
            'password' => 'Password salah atau tidak ditemukan',
        ]);
    }
    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img('math')]);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        // dd(session()->all());
        return redirect('/');
        //return redirect('users');
    }
}
