<?php

namespace App\Http\Controllers;

use App\Models\Dokumen;
use App\Models\Permohonan;
use Illuminate\Http\Request;

class VerifikasiController extends Controller
{
    public function index($uuid)
    {

        $dokumen = Dokumen::where('uuid', $uuid)->first();
        $attach = Dokumen::where('permohonan_id', $dokumen->permohonan_id)->where('jenisdokumen_id', 4)->first();

        // $permohonan = Permohonan::findorfail($uuid);

        return view('publik.tanda-tangan', compact('dokumen', 'attach'));
    }
}
