<?php

namespace App\Http\Controllers;

use App\Models\Permohonan;
use App\Models\Posisi;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StatusController extends Controller
{
    public function index()
    {
        return view('publik.status');
    }

    public function cek(Request $request)
    {

        $request->validate(
            [
                'token' => 'required'
            ],
            [
                'token.required' => 'Token tidak boleh kosong',
            ]
        );


        try {
            $permohonan = Permohonan::where('token_pemohon', $request->token)->firstorfail();
            // dd($permohonan->token_pemohon);
            return redirect('/hasil-cek/' . $permohonan->token_pemohon);
        } catch (ModelNotFoundException $e) {
            return redirect('/status')->with('toast_error', 'Token tidak ditemukan');
        }
    }

    public function hasilCek($token_pemohon)
    {
        $permohonan = Permohonan::where('token_pemohon', $token_pemohon)->firstorfail();
        $posisi = Posisi::all();

        return view('publik.hasil-cek', compact('permohonan', 'posisi'));
    }
}
