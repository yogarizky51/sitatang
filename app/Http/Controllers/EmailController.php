<?php

namespace App\Http\Controllers;

use App\Models\Permohonan;
use App\Models\User;
use App\Notifications\AdminKoorNotification;
use App\Notifications\PermohonanNotification;
use App\Notifications\StatusNotification;
use App\Notifications\TokenNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Notification;

class EmailController extends Controller
{
    public function tokenEmail($uuid)
    {
        // $pemohon = Permohonan::findorfail($id);
        $pemohon = Permohonan::where('uuid', $uuid)->first();
        $roleId = $pemohon->pengguna->role_id_utama;
        // $roleIdKedua = $pemohon->pengguna->role_id_;
        $user = User::where('pengguna_id', $pemohon->pengguna_id)->first();
        $admin = User::where('role_id_utama', 2)->first();
        $koorprodi = User::where('role_id_utama', 3)->first();
        $data_email = [
            'greeting' => 'Halo ' . $pemohon->nama_pemohon . ',',
            'body' => 'Ini adalah token untuk melakukan pengecekan terhadap permohonan yang diajukan.',
            'token' => 'Token Permohonan Anda: ' . $pemohon->token_pemohon,
            'thanks' => 'Mohon untuk tidak menghapus pesan ini kecuali sudah menyimpan token diatas, Terima Kasih',
        ];
        $data_email_pemohon = [
            'subject' => '(SiTaTang) Ada permohonan tanda tangan dari ' . $pemohon->nama_pemohon,
            'greeting' => 'Halo ' . $user->nama . ',',
            'body' => new HtmlString('Ada permohonan tanda tangan masuk dengan nama pemohon yaitu: <strong>' . $pemohon->nama_pemohon .  '</strong>. dan judul surat <strong>' . $pemohon->judul_surat . '</strong>.'),
            'thanks' => 'Mohon untuk memproses terkait permohonan tanda tangan yang masuk, Terima Kasih',
        ];
        $data_email_admin = [
            'subject' => '(SiTaTang) Ada permohonan tanda tangan untuk koorprodi dari ' . $pemohon->nama_pemohon,
            'greeting' => 'Halo ' . $admin->nama . ',',
            'body' => new HtmlString('Ada permohonan tanda tangan untuk koorprodi PTIK dengan nama pemohon yaitu: <strong>' . $pemohon->nama_pemohon .  '</strong>. dan judul surat <strong>' . $pemohon->judul_surat . '</strong>.'),
            'thanks' => 'Mohon untuk melakukan pengecekan dan memproses permohonan yang masuk, Terima Kasih',
        ];

        $email = $user->email;
        $email_koor = $koorprodi->email;
        $tujuan = $pemohon->email_pemohon;

        if ($roleId == 3) {
            Notification::route('mail', $email_koor)->notify(new PermohonanNotification($data_email_pemohon));
        } else {
            Notification::route('mail', $email)->notify(new PermohonanNotification($data_email_pemohon));
        }

        Notification::route('mail', $tujuan)->notify(new TokenNotification($data_email));

        // dd($roleId);
        return redirect('/fill-data/' . $uuid);
    }

    public function tokenEmailKoor($uuid)
    {
        // $pemohon = Permohonan::findorfail($id);
        $pemohon = Permohonan::where('uuid', $uuid)->first();
        $roleId = $pemohon->pengguna->role_id_utama;
        $roleIdKedua = $pemohon->pengguna->role_id_;
        $user = User::where('pengguna_id', $pemohon->pengguna_id)->first();
        $admin = User::where('role_id_utama', 2)->first();
        $data_email = [
            'greeting' => 'Halo ' . $pemohon->nama_pemohon . ',',
            'body' => 'Ini adalah token untuk melakukan pengecekan terhadap permohonan yang diajukan.',
            'token' => 'Token Permohonan Anda: ' . $pemohon->token_pemohon,
            'thanks' => 'Mohon untuk tidak menghapus pesan ini kecuali sudah menyimpan token diatas, Terima Kasih',
        ];
        // $data_email_pemohon = [
        //     'subject' => '(SiTaTang) Ada permohonan tanda tangan dari ' . $pemohon->nama_pemohon,
        //     'greeting' => 'Halo ' . $user->nama . ',',
        //     'body' => new HtmlString('Ada permohonan tanda tangan masuk dengan nama pemohon yaitu: <strong>' . $pemohon->nama_pemohon .  '</strong>. dan judul surat <strong>' . $pemohon->judul_surat . '</strong>.'),
        //     'thanks' => 'Mohon untuk memproses terkait permohonan tanda tangan yang masuk, Terima Kasih',
        // ];
        $data_email_admin = [
            'subject' => '(SiTaTang) Ada permohonan tanda tangan untuk koorprodi dari ' . $pemohon->nama_pemohon,
            'greeting' => 'Halo ' . $admin->nama . ',',
            'body' => new HtmlString('Ada permohonan tanda tangan untuk koorprodi PTIK dengan nama pemohon yaitu: <strong>' . $pemohon->nama_pemohon .  '</strong>. dan judul surat <strong>' . $pemohon->judul_surat . '</strong>.'),
            'thanks' => 'Mohon untuk melakukan pengecekan dan memproses permohonan yang masuk, Terima Kasih',
        ];

        // $email = $user->email;
        $email_admin = $admin->email;
        $tujuan = $pemohon->email_pemohon;

        Notification::route('mail', $email_admin)->notify(new AdminKoorNotification($data_email_admin));


        Notification::route('mail', $tujuan)->notify(new TokenNotification($data_email));

        // dd($roleId);
        return redirect('/fill-data/' . $uuid);
    }

    public function emailSetuju($id)
    {
        $permohonan = Permohonan::findorfail($id);

        $data_email_selesai = [
            'greeting' => 'Halo ' . $permohonan->nama_pemohon . ',',
            'body' => new HtmlString('Status permohonan dengan judul surat: <strong>' . $permohonan->judul_surat . '</strong> yaitu: '),
            'status' => new HtmlString('<strong>Disetujui</strong>'),
            'thanks' => 'Mohon melakukan pengecekan di web SiTaTang untuk informasi lebih lanjut, Terima Kasih',
        ];

        $tujuan = $permohonan->email_pemohon;

        Notification::route('mail', $tujuan)->notify(new StatusNotification($data_email_selesai));

        if (Auth::user()->role_id_utama == 3) {
            if ($permohonan->role_id == 4) {
                return redirect('/daftar-permohonan-koordosen')->with('toast_success', 'Pengajuan berhasil disetujui');
            }
            return redirect('/daftar-permohonan-koorprodi')->with('toast_success', 'Pengajuan berhasil disetujui');
        } else {
            return redirect('/daftar-permohonan')->with('toast_success', 'Pengajuan berhasil disetujui');
        }
    }

    public function emailDitolak($id)
    {
        $permohonan = Permohonan::findorfail($id);

        $data_email_selesai = [
            'greeting' => 'Halo ' . $permohonan->nama_pemohon . ',',
            'body' => new HtmlString('Status permohonan dengan judul surat: <strong>' . $permohonan->judul_surat . '</strong> yaitu: '),
            'status' => new HtmlString('<strong>Ditolak</strong>'),
            'thanks' => 'Mohon melakukan pengecekan di web SiTaTang untuk informasi lebih lanjut, Terima Kasih',
        ];

        $tujuan = $permohonan->email_pemohon;

        Notification::route('mail', $tujuan)->notify(new StatusNotification($data_email_selesai));

        if (Auth::user()->role_id_utama == 3) {
            if ($permohonan->role_id == 4) {
                return redirect('/daftar-permohonan-koordosen')->with('toast_success', 'Pengajuan berhasil ditolak');
            }
            return redirect('/daftar-permohonan-koorprodi')->with('toast_success', 'Pengajuan berhasil ditolak');
        } else if ($permohonan->pengguna->role_id_utama == 3) {
            return redirect('/koorprodi-permohonan')->with('toast_success', 'Pengajuan berhasil ditolak');
        } else {
            return redirect('/daftar-permohonan')->with('toast_success', 'Pengajuan berhasil ditolak');
        }
    }

    public function adminSetuju($id)
    {
        $pemohon = Permohonan::findorfail($id);
        $user = User::where('pengguna_id', $pemohon->pengguna_id)->first();

        $data_email_pemohon = [
            'subject' => '(SiTaTang) Ada permohonan tanda tangan untuk koorprodi dari ' . $pemohon->nama_pemohon,
            'greeting' => 'Halo ' . $user->nama . ',',
            'body' => new HtmlString('Ada permohonan tanda tangan masuk dengan nama pemohon yaitu: <strong>' . $pemohon->nama_pemohon .  '</strong>. dan judul surat <strong>' . $pemohon->judul_surat . '</strong>.'),
            'thanks' => 'Mohon untuk memproses terkait permohonan tanda tangan yang masuk, Terima Kasih',
        ];

        $email = $user->email;

        Notification::route('mail', $email)->notify(new PermohonanNotification($data_email_pemohon));

        return redirect('/koorprodi-permohonan')->with('toast_success', 'Pengajuan berhasil dilanjut kepada koorprodi');
    }
}
