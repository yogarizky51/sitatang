<?php

namespace App\Http\Controllers;

use App\Models\Permohonan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::where('role_id_utama', 3)->first()->pengguna_id;
        // $user_admin = User::where('role_id_utama', 2)->first()->pengguna_id;
        $pengajuan = Permohonan::where("pengguna_id", Auth::user()->pengguna_id)->where('status_id', 1)->get()->count();
        $adminsetuju = Permohonan::where("pengguna_id", Auth::user()->pengguna_id)->where('status_id', 4)->get()->count();
        $dilanjutkankekoorprodi = Permohonan::where("pengguna_id", $users)->where('status_id', 4)->get()->count();
        $koordosen = Permohonan::where("pengguna_id", Auth::user()->pengguna_id)->where('status_id', 1)->where('role_id', 4)->get()->count();
        $admin = Permohonan::where("pengguna_id", $users)->where('role_id', 3)->where('status_id', 1)->get()->count();
        $setuju = Permohonan::where("pengguna_id", Auth::user()->pengguna_id)->where('status_id', 2)->get()->count();
        $ditolak = Permohonan::where("pengguna_id", Auth::user()->pengguna_id)->where('status_id', 3)->get()->count();

        $data = [
            'adminsetuju' => $adminsetuju,
            'admin' => $admin,
            'pengajuan' => $pengajuan,
            'setuju' => $setuju,
            'ditolak'   => $ditolak,
            'koordosen'   => $koordosen,
            'dilanjutkankekoorprodi' => $dilanjutkankekoorprodi
        ];
        return view('admin.dashboard', $data);
    }
}
