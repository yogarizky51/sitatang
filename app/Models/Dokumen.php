<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    use HasFactory;

    protected $table = "dokumen";
    protected $fillable = [
        'uuid',
        'permohonan_id',
        'jenisdokumen_id',
        'filename',
        'rename',
        'filesize',
        'mimetype',
        'extension',
        'path',
    ];
    protected $primaryKey = 'dokumen_id';

    public function jenisDokumen() {
        return $this->belongsTo(JenisDokumen::class, 'jenisdokumen_id');
    }

    public function permohonan() {
        return $this->belongsTo(Permohonan::class, 'permohonan_id');
    }
}
