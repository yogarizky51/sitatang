<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    use HasFactory;

    protected $table = "posisi";
    protected $fillable = ['letak_posisi'];
    protected $primaryKey = 'posisi_id';

    public function permohonan()
    {
        return $this->hasMany(Permohonan::class, 'posisi_id');
    }
}
