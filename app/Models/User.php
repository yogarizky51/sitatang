<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = "pengguna";
    protected $fillable = [
        'biodata_id',
        'role_id_utama',
        'role_id_kedua',
        'uuid',
        'nama',
        'email',
        'password',
    ];
    protected $primaryKey = 'pengguna_id';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function biodata()
    {
        return $this->belongsTo(Biodata::class, 'biodata_id');
    }

    public function role_utama()
    {
        return $this->belongsTo(Role::class, 'role_id_utama', 'role_id');
    }

    public function role_kedua()
    {
        return $this->belongsTo(Role::class, 'role_id_kedua', 'role_id');
    }

    public function permohonan()
    {
        return $this->hasOne(Permohonan::class, 'permohonan_id');
    }
}
