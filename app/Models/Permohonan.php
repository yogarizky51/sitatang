<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Permohonan extends Model
{
    use HasFactory, Notifiable;

    protected $table = "permohonan";
    protected $fillable = [
        'status_id',
        'pengguna_id',
        'posisi_id',
        'role_id',
        'nama_pemohon',
        'email_pemohon',
        'judul_surat',
        'token_Pemohon',
        'pesan',
        'note',
    ];
    protected $primaryKey = 'permohonan_id';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function pengguna()
    {
        return $this->belongsTo(User::class, 'pengguna_id');
    }
    public function posisi()
    {
        return $this->belongsTo(Posisi::class, 'posisi_id');
    }
    public function dokumen()
    {
        return $this->hasMany(Permohonan::class, 'permohonan_id');
    }
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
