<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    use HasFactory;

    protected $table = "biodata_pengguna";
    protected $fillable = ['nip'];
    protected $primaryKey = 'biodata_id';
    
    public function biodata_pengguna() {
        return $this->hasOne(User::class, 'biodata_id');
    }
}
