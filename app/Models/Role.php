<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = "role";
    protected $fillable = ['nama'];
    protected $primaryKey = 'role_id';

    public function rolePengguna()
    {
        return $this->hasMany(RolePengguna::class, 'akses_id');
    }

    public function pengguna_utama()
    {
        return $this->hasMany(User::class, 'role_id_utama');
    }

    public function pengguna_kedua()
    {
        return $this->hasMany(User::class, 'role_id_kedua');
    }
    public function permohonan()
    {
        return $this->hasMany(Permohonan::class, 'role_id');
    }
}
