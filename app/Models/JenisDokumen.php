<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisDokumen extends Model
{
    use HasFactory;

    protected $table = "jenis_dokumen";
    protected $fillable = [
        'nama',
    ];
    protected $primaryKey = 'jenisdokumen_id';

    public function jenisDokumen_dokumen() {
        return $this->hasMany(Dokumen::class, 'jenisdokumen_id');
    }
}
