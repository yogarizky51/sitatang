<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePengguna extends Model
{
    use HasFactory;

    protected $table = "role_pengguna";
    protected $fillable = [
        'akses_id',
        'pengguna_id',
        'aktif',
    ];
    protected $primaryKey = 'rolepengguna_id';
    
    public function role() {
        return $this->belongsTo(Role::class, 'akses_id');
    }
    public function pengguna()
    {
        return $this->belongsTo(User::class, 'pengguna_id');
    }
}
