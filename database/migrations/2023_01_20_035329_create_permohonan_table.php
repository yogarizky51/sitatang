<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan', function (Blueprint $table) {
            $table->id('permohonan_id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('pengguna_id');
            $table->unsignedBigInteger('posisi_id')->nullable();
            $table->unsignedBigInteger('role_id');
            $table->string('nama_pemohon');
            $table->string('email_pemohon');
            $table->string('judul_surat');
            $table->string('judul_attachment')->nullable();
            $table->string('token_pemohon');
            $table->string('pesan');
            $table->longtext('qr_code')->nullable();
            $table->longtext('qr_name')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
            $table->foreign('role_id')->references('role_id')->on('role')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pengguna_id')->references('pengguna_id')->on('pengguna')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id')->references('status_id')->on('status')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('posisi_id')->references('posisi_id')->on('posisi')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permohonan');
    }
}
