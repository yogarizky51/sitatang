<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenggunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengguna', function (Blueprint $table) {
            $table->id('pengguna_id');
            $table->unsignedBigInteger('biodata_id');
            $table->unsignedBigInteger('role_id_utama');
            $table->unsignedBigInteger('role_id_kedua')->nullable();
            $table->uuid('uuid')->nullable();;
            $table->string('nama');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('biodata_id')->references('biodata_id')->on('biodata_pengguna')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id_utama')->references('role_id')->on('role')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id_kedua')->references('role_id')->on('role')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
