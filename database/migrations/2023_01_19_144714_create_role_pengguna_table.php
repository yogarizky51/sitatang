<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePenggunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_pengguna', function (Blueprint $table) {
            $table->id('rolepengguna_id');
            $table->unsignedBigInteger('akses_id');
            $table->unsignedBigInteger('pengguna_id');
            $table->tinyInteger('aktif')->nullable();
            $table->timestamps();
            $table->foreign('akses_id')->references('role_id')->on('role')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pengguna_id')->references('pengguna_id')->on('pengguna')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_pengguna');
    }
}
