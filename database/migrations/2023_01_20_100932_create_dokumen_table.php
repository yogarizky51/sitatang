<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDokumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen', function (Blueprint $table) {
            $table->id('dokumen_id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('permohonan_id');
            $table->unsignedBigInteger('jenisdokumen_id');
            $table->string('file_name');
            $table->string('rename');
            $table->string('file_size');
            $table->string('mime_type');
            $table->string('extension');
            $table->string('path');
            $table->timestamps();
            $table->foreign('permohonan_id')->references('permohonan_id')->on('permohonan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('jenisdokumen_id')->references('jenisdokumen_id')->on('jenis_dokumen')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen');
    }
}
