<?php

namespace Database\Seeders;

use App\Models\Biodata;
use App\Models\JenisDokumen;
use App\Models\Posisi;
use App\Models\Role;
use App\Models\RolePengguna;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $biodatas = [111, 222, 333, 444];
        foreach ($biodatas as $biodata) {
            Biodata::create([
                'nip' => $biodata
            ]);
        }

        $roles = ['Super User', 'Admin', 'Koorprodi', 'Dosen', 'Tidak Merangkap'];
        foreach ($roles as $role) {
            Role::create([
                'nama' => $role
            ]);
        }

        for ($i = 1; $i <= 120; $i++) {
            Posisi::create([
                "letak_posisi" => $i,
            ]);
        }

        #superuser
        $data = [
            'nama'      => 'superuser',
            'uuid'      => Str::uuid()->toString(),
            'password'  => Hash::make('asd'),
            'email'     => 'saworun@mailinator.com',
            'biodata_id' => 1,
            'role_id_utama' => 1,
            'role_id_kedua' => 5
        ];
        User::create($data);

        #koorprodi
        $data = [
            'nama'      => 'User Koorprodi',
            'uuid'      => Str::uuid()->toString(),
            'password'  => Hash::make('asd'),
            'email'     => 'koorprodi@mailinator.com',
            'biodata_id' => 2,
            'role_id_utama' => 3,
            'role_id_kedua' => 4
        ];
        User::create($data);

        #adminprodi
        $data = [
            'nama'      => 'User Admin',
            'uuid'      => Str::uuid()->toString(),
            'password'  => Hash::make('asd'),
            'email'     => 'admin@mailinator.com',
            'biodata_id' => 3,
            'role_id_utama' => 2,
            'role_id_kedua' => 5
        ];
        User::create($data);

        #dosen
        $data = [
            'nama'      => 'User Dosen',
            'uuid'      => Str::uuid()->toString(),
            'password'  => Hash::make('asd'),
            'email'     => 'dosen@mailinator.com',
            'biodata_id' => 4,
            'role_id_utama' => 4,
            'role_id_kedua' => 5
        ];
        User::create($data);

        $status   = ['Dalam Pengajuan', 'Disetujui', 'Ditolak', 'Disetujui Admin'];
        $labels   = ['info', 'warning', 'danger', 'success'];
        $i = 0;
        foreach ($status as $item) {
            Status::create([
                'nama' => $item,
                'label' => 'label-' . $labels[$i++]
            ]);
        }

        $jenis_dokumen  = ['[PERMOHONAN]', '[DISETUJUI]', '[DITOLAK]', '[ATTACHMENT]'];
        foreach ($jenis_dokumen as $item) {
            JenisDokumen::create(['nama' => $item]);
        }

        #role_pengguna_superuser
        $data = [
            'akses_id'      => 1,
            'pengguna_id'   => 1,
            'aktif'         => 1
        ];
        RolePengguna::create($data);

        #role_pengguna_admin
        $data = [
            'akses_id'      => 2,
            'pengguna_id'   => 3,
            'aktif'         => 1
        ];
        RolePengguna::create($data);

        #role_pengguna_admin
        $data = [
            'akses_id'      => 3,
            'pengguna_id'   => 2,
            'aktif'         => 1
        ];
        RolePengguna::create($data);

        #role_pengguna_admin
        $data = [
            'akses_id'      => 4,
            'pengguna_id'   => 4,
            'aktif'         => 1
        ];
        RolePengguna::create($data);
    }
}
